# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181013174033) do

  create_table "algorithm_moneys", force: :cascade do |t|
    t.integer "algorithm_id"
    t.integer "money_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["algorithm_id"], name: "index_algorithm_moneys_on_algorithm_id"
    t.index ["money_id"], name: "index_algorithm_moneys_on_money_id"
  end

  create_table "algorithms", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "body"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "article_categories", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.string "slug"
    t.string "meta_desc"
    t.string "meta_key"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.text "body"
    t.string "meta_key"
    t.string "meta_desc"
    t.integer "user_id"
    t.integer "article_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_category_id"], name: "index_articles_on_article_category_id"
    t.index ["user_id"], name: "index_articles_on_user_id"
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "commontator_comments", force: :cascade do |t|
    t.string "creator_type"
    t.integer "creator_id"
    t.string "editor_type"
    t.integer "editor_id"
    t.integer "thread_id", null: false
    t.text "body", null: false
    t.datetime "deleted_at"
    t.integer "cached_votes_up", default: 0
    t.integer "cached_votes_down", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cached_votes_down"], name: "index_commontator_comments_on_cached_votes_down"
    t.index ["cached_votes_up"], name: "index_commontator_comments_on_cached_votes_up"
    t.index ["creator_id", "creator_type", "thread_id"], name: "index_commontator_comments_on_c_id_and_c_type_and_t_id"
    t.index ["thread_id", "created_at"], name: "index_commontator_comments_on_thread_id_and_created_at"
  end

  create_table "commontator_subscriptions", force: :cascade do |t|
    t.string "subscriber_type", null: false
    t.integer "subscriber_id", null: false
    t.integer "thread_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subscriber_id", "subscriber_type", "thread_id"], name: "index_commontator_subscriptions_on_s_id_and_s_type_and_t_id", unique: true
    t.index ["thread_id"], name: "index_commontator_subscriptions_on_thread_id"
  end

  create_table "commontator_threads", force: :cascade do |t|
    t.string "commontable_type"
    t.integer "commontable_id"
    t.datetime "closed_at"
    t.string "closer_type"
    t.integer "closer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commontable_id", "commontable_type"], name: "index_commontator_threads_on_c_id_and_c_type", unique: true
  end

  create_table "events", force: :cascade do |t|
    t.integer "money_id"
    t.string "name"
    t.string "slug"
    t.text "description"
    t.text "body"
    t.string "status_text"
    t.integer "progress"
    t.string "image"
    t.datetime "appointment_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["money_id"], name: "index_events_on_money_id"
  end

  create_table "exchange_courses", force: :cascade do |t|
    t.integer "exchange_id"
    t.integer "money_from_id"
    t.integer "money_to_id"
    t.string "currency_from"
    t.string "currency_to"
    t.decimal "amount_min"
    t.decimal "amount_max"
    t.decimal "amount_exist"
    t.decimal "value_in"
    t.decimal "value_out"
    t.string "params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["exchange_id"], name: "index_exchange_courses_on_exchange_id"
    t.index ["money_from_id"], name: "index_exchange_courses_on_money_from_id"
    t.index ["money_to_id"], name: "index_exchange_courses_on_money_to_id"
  end

  create_table "exchanges", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.text "body"
    t.string "website"
    t.string "partner_link"
    t.integer "status"
    t.string "image"
    t.string "course_link"
    t.string "parser_type", default: "default"
    t.datetime "birth_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "error_count"
    t.string "error_text"
  end

  create_table "explorers", force: :cascade do |t|
    t.string "name"
    t.text "body"
    t.string "slug"
    t.string "image"
    t.string "website"
    t.string "api_url"
    t.string "api_type"
    t.integer "money_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["money_id"], name: "index_explorers_on_money_id"
  end

  create_table "go_counters", force: :cascade do |t|
    t.string "resource_class"
    t.integer "resource_id"
    t.datetime "date_go"
    t.text "user_agent"
    t.string "ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "identities", force: :cascade do |t|
    t.integer "user_id"
    t.string "provider"
    t.string "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "market_moneys", force: :cascade do |t|
    t.integer "market_id"
    t.integer "money_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["market_id"], name: "index_market_moneys_on_market_id"
    t.index ["money_id"], name: "index_market_moneys_on_money_id"
  end

  create_table "markets", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.string "website"
    t.string "partner_link"
    t.string "image"
    t.string "language"
    t.text "body"
    t.string "twitter"
    t.integer "position", default: 1000
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "miner_modes", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "image"
    t.text "body"
    t.integer "miner_id"
    t.string "website"
    t.string "partner_link"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["miner_id"], name: "index_miner_modes_on_miner_id"
  end

  create_table "miner_speeds", force: :cascade do |t|
    t.integer "miner_id"
    t.text "body"
    t.integer "hash_rate"
    t.integer "hash_rate_dual"
    t.integer "power_rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "algorithm_id"
    t.integer "algorithm_dual_id"
    t.index ["algorithm_dual_id"], name: "index_miner_speeds_on_algorithm_dual_id"
    t.index ["algorithm_id"], name: "index_miner_speeds_on_algorithm_id"
    t.index ["miner_id"], name: "index_miner_speeds_on_miner_id"
  end

  create_table "miners", force: :cascade do |t|
    t.string "brand_name"
    t.string "name"
    t.string "slug"
    t.integer "miner_type"
    t.text "body"
    t.string "image"
    t.integer "power_rate"
    t.integer "noise_level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "money", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "body"
    t.string "last_updated"
    t.string "website"
    t.string "symbol"
    t.string "currency"
    t.string "image"
    t.string "twitter"
    t.integer "money_type_id", default: 0
    t.integer "position", default: 1000
    t.integer "change_info_coefficient", default: 1
    t.integer "status"
    t.integer "fork_parent"
    t.integer "fork_date"
    t.decimal "price_usd"
    t.decimal "price_btc"
    t.decimal "day_volume_usd"
    t.decimal "market_cap_usd"
    t.decimal "available_supply"
    t.decimal "total_supply"
    t.decimal "max_supply"
    t.decimal "percent_change_1h"
    t.decimal "percent_change_24h"
    t.decimal "percent_change_7d"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "news", force: :cascade do |t|
    t.string "title"
    t.text "meta_description"
    t.text "meta_keywors"
    t.string "slug"
    t.text "body"
    t.string "source"
    t.string "image"
    t.integer "user_id"
    t.integer "status"
    t.string "link"
    t.integer "read_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_news_on_user_id"
  end

  create_table "news_sources", force: :cascade do |t|
    t.string "name"
    t.string "source"
    t.string "website"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.text "meta_description"
    t.text "meta_keywords"
    t.text "body"
    t.string "h1"
    t.string "controller"
    t.string "action"
    t.text "simple_text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pool_moneys", force: :cascade do |t|
    t.integer "pool_id"
    t.integer "money_id"
    t.text "body"
    t.string "website"
    t.string "partner_link"
    t.decimal "pool_hashrate"
    t.integer "miners_count"
    t.integer "workers_count"
    t.decimal "blocks_hour"
    t.integer "mining_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["money_id"], name: "index_pool_moneys_on_money_id"
    t.index ["pool_id"], name: "index_pool_moneys_on_pool_id"
  end

  create_table "pools", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "body"
    t.string "website"
    t.string "partner_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "socials", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "image"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "telegram_chanels", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "body"
    t.string "website"
    t.integer "count_user"
    t.integer "type_chanel"
    t.string "partner_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "social_id"
    t.integer "paid"
    t.index ["social_id"], name: "index_telegram_chanels_on_social_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.string "avatar"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "wallet_moneys", force: :cascade do |t|
    t.integer "wallet_id"
    t.integer "money_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["money_id"], name: "index_wallet_moneys_on_money_id"
    t.index ["wallet_id"], name: "index_wallet_moneys_on_wallet_id"
  end

  create_table "wallets", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "body"
    t.string "website"
    t.string "partner_link"
    t.integer "wallet_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "youtube_chanels", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "body"
    t.string "image"
    t.integer "count_user"
    t.string "chanel_id"
    t.string "website"
    t.string "partner_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "social_id"
    t.index ["social_id"], name: "index_youtube_chanels_on_social_id"
  end

end
