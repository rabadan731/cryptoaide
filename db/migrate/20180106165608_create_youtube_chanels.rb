class CreateYoutubeChanels < ActiveRecord::Migration[5.1]
  def change
    create_table :youtube_chanels do |t|
      t.string :name
      t.string :slug
      t.text :body
      t.string :image
      t.integer :count_user
      t.string :chanel_id
      t.string :website
      t.string :partner_link

      t.timestamps
    end
  end
end
