class CreateMarkets < ActiveRecord::Migration[5.1]
  def change
    create_table :markets do |t|
      t.string :title
      t.string :slug
      t.string :website
      t.string :partner_link
      t.string :image
      t.string :language
      t.text :body
      t.string :twitter
      t.integer :position, :default => 1000

      t.timestamps
    end
  end
end
