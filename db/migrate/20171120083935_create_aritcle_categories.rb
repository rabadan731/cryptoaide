class CreateAritcleCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :article_categories do |t|
      t.string :title
      t.text :body
      t.string :slug
      t.string :meta_desc
      t.string :meta_key
      t.string :image

      t.timestamps
    end
  end
end
