class CreateExplorers < ActiveRecord::Migration[5.1]
  def change
    create_table :explorers do |t|
      t.string :name
      t.text :body
      t.string :slug
      t.string :image
      t.string :website
      t.string :api_url
      t.string :api_type
      t.references :money, foreign_key: true

      t.timestamps
    end
  end
end
