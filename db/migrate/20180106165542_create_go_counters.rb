class CreateGoCounters < ActiveRecord::Migration[5.1]
  def change
    create_table :go_counters do |t|
      t.string :resource_class
      t.integer :resource_id
      t.datetime :date_go
      t.text :user_agent
      t.string :ip

      t.timestamps
    end
  end
end
