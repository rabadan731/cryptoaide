class CreateNews < ActiveRecord::Migration[5.1]
  def change
    create_table :news do |t|
      t.string :title
      t.text :meta_description
      t.text :meta_keywors
      t.string :slug
      t.text :body
      t.string :source
      t.string :image
      t.references :user, foreign_key: true
      t.integer :status

      t.string :link
      t.integer :read_count

      t.timestamps
    end
  end
end
