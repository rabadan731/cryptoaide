class CreateWalletMoneys < ActiveRecord::Migration[5.1]
  def change
    create_table :wallet_moneys do |t|
      t.references :wallet, foreign_key: true
      t.references :money, foreign_key: true

      t.timestamps
    end
  end
end
