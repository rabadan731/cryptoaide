class CreatePoolMoneys < ActiveRecord::Migration[5.1]
  def change
    create_table :pool_moneys do |t|
      t.references :pool, foreign_key: true
      t.references :money, foreign_key: true
      t.text :body
      t.string :website
      t.string :partner_link
      t.numeric :pool_hashrate
      t.integer :miners_count
      t.integer :workers_count
      t.numeric :blocks_hour
      t.integer :mining_type

      t.timestamps
    end
  end
end
