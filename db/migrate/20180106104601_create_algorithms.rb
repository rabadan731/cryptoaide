class CreateAlgorithms < ActiveRecord::Migration[5.1]
  def change
    create_table :algorithms do |t|
      t.string :name
      t.string :slug
      t.text :body
      t.string :image

      t.timestamps
    end
  end
end
