class CreateMiners < ActiveRecord::Migration[5.1]
  def change
    create_table :miners do |t|
      t.string :brand_name
      t.string :name
      t.string :slug
      t.integer :miner_type
      t.text :body
      t.string :image
      t.integer :power_rate
      t.integer :noise_level

      t.timestamps
    end
  end
end
