class CreateExchangeCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :exchange_courses do |t|
      t.references :exchange, foreign_key: true
      t.references :money_from, foreign_key: { to_table: :money }, references: :money
      t.references :money_to, foreign_key:  { to_table: :money }, references: :money
      t.string :currency_from
      t.string :currency_to
      t.numeric :amount_min
      t.numeric :amount_max
      t.numeric :amount_exist
      t.numeric :value_in
      t.numeric :value_out
      t.string :params

      t.timestamps
    end
  end
end
