class CreateMinerSpeeds < ActiveRecord::Migration[5.1]
  def change
    create_table :miner_speeds do |t|
      t.references :miner, foreign_key: true
      t.references :money, foreign_key: true
      t.references :money_dual, foreign_key: { to_table: :money }, references: :money
      t.text :body
      t.integer :hash_rate
      t.integer :hash_rate_dual
      t.integer :power_rate

      t.timestamps
    end
  end
end
