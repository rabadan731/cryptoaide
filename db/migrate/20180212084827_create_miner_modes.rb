class CreateMinerModes < ActiveRecord::Migration[5.1]
  def change
    create_table :miner_modes do |t|
      t.string :name
      t.string :slug
      t.string :image
      t.text :body
      t.references :miner, foreign_key: true
      t.string :website
      t.string :partner_link
      t.float :price

      t.timestamps
    end
  end
end
