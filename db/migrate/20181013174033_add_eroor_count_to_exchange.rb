class AddEroorCountToExchange < ActiveRecord::Migration[5.1]
  def change
    add_column :exchanges, :error_count, :integer
    add_column :exchanges, :error_text, :string
  end
end
