class RemoveMoneyFromMinerSpeeds < ActiveRecord::Migration[5.1]
  def change
    remove_reference :miner_speeds, :money, foreign_key: true
    remove_reference :miner_speeds, :money_dual, foreign_key: true
    add_reference :miner_speeds, :algorithm, foreign_key: true
    add_reference :miner_speeds, :algorithm_dual, foreign_key: true
  end
end
