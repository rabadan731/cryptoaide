class CreateNewsSources < ActiveRecord::Migration[5.1]
  def change
    create_table :news_sources do |t|
      t.string :name
      t.string :source
      t.string :website
      t.integer :status

      t.timestamps
    end
  end
end
