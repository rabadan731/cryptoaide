class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :slug
      t.text :body
      t.string :meta_key
      t.string :meta_desc

      t.references :user, foreign_key: true
      t.references :article_category, foreign_key: true

      t.timestamps
    end
  end
end
