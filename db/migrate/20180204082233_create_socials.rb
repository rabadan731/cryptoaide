class CreateSocials < ActiveRecord::Migration[5.1]
  def change
    create_table :socials do |t|
      t.string :name
      t.string :slug
      t.string :image
      t.text :body

      t.timestamps
    end

    add_reference :youtube_chanels, :social, foreign_key: true
    add_reference :telegram_chanels, :social, foreign_key: true
    add_column :telegram_chanels, :paid, :integer
  end
end
