class CreateAlgorithmMoneys < ActiveRecord::Migration[5.1]
  def change
    create_table :algorithm_moneys do |t|
      t.references :algorithm, foreign_key: true
      t.references :money, foreign_key: true

      t.timestamps
    end
  end
end
