class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :name
      t.string :title
      t.text :meta_description
      t.text :meta_keywords
      t.text :body
      t.string :h1
      t.string :controller
      t.string :action
      t.text :simple_text

      t.timestamps
    end
  end
end
