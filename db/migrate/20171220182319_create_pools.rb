class CreatePools < ActiveRecord::Migration[5.1]
  def change
    create_table :pools do |t|
      t.string :name
      t.string :slug
      t.text :body
      t.string :website
      t.string :partner_link

      t.timestamps
    end
  end
end
