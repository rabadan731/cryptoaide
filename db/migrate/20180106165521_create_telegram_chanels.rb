class CreateTelegramChanels < ActiveRecord::Migration[5.1]
  def change
    create_table :telegram_chanels do |t|
      t.string :name
      t.string :slug
      t.text :body
      t.string :website
      t.integer :count_user
      t.integer :type_chanel
      t.string :partner_link

      t.timestamps
    end
  end
end
