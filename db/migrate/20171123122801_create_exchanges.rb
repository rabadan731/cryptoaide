class CreateExchanges < ActiveRecord::Migration[5.1]
  def change
    create_table :exchanges do |t|
      t.string :title
      t.string :slug
      t.text :body
      t.string :website
      t.string :partner_link
      t.integer :status
      t.string :image

      t.string :course_link
      t.string :parser_type, :default => 'default'
      # t.references :country, foreign_key: true
      t.datetime :birth_date

      t.timestamps
    end
  end
end
