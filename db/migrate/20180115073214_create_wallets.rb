class CreateWallets < ActiveRecord::Migration[5.1]
  def change
    create_table :wallets do |t|
      t.string :name
      t.string :slug
      t.text :body
      t.string :website
      t.string :partner_link
      t.integer :wallet_type

      t.timestamps
    end
  end
end
