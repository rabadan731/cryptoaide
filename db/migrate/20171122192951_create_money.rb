class CreateMoney < ActiveRecord::Migration[5.1]
  def change
    create_table :money do |t|
      t.string :name
      t.string :slug
      t.text :body

      t.string :last_updated
      t.string :website
      t.string :symbol
      t.string :currency
      t.string :image
      t.string :twitter
      t.integer :money_type_id, :default => 0
      t.integer :position, :default => 1000
      t.integer :change_info_coefficient, :default => 1
      t.integer :status

      t.integer :fork_parent
      t.integer :fork_date

      t.numeric :price_usd
      t.numeric :price_btc
      t.numeric :day_volume_usd
      t.numeric :market_cap_usd
      t.numeric :available_supply
      t.numeric :total_supply
      t.numeric :max_supply
      t.numeric :percent_change_1h
      t.numeric :percent_change_24h
      t.numeric :percent_change_7d

      t.timestamps
    end
  end
end
