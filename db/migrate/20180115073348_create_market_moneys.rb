class CreateMarketMoneys < ActiveRecord::Migration[5.1]
  def change
    create_table :market_moneys do |t|
      t.references :market, foreign_key: true
      t.references :money, foreign_key: true

      t.timestamps
    end
  end
end
