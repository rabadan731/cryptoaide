class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.references :money, foreign_key: true
      t.string :name
      t.string :slug
      t.text :description
      t.text :body
      t.string :status_text
      t.integer :progress
      t.string :image
      t.datetime :appointment_date

      t.timestamps
    end
  end
end
