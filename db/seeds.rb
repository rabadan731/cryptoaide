# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create!(
  :name                 => "rabadan731",
  :email                 => "rabadan731@yandex.ru",
  :password              => "rabadan731",
  :password_confirmation => "rabadan731"
)
user.admin!


10.times do |i|
  News.create!(title: "News title #{i} #{i}",
                      status: 7,
                      meta_description: 'db:seed command (or created alongside the database with. This file should',
                      body: 'This file should contain all the record creation needed to seed the database with its default values.
The data can then be loaded with the rails db:seed command (or created alongside the database with. This file should contain all the record creation needed to seed the database with its default values.
The data can then be loaded with the rails db:seed command (or created alongside the database with. This file should contain all the record creation needed to seed the database with its default values.
The data can then be loaded with the rails db:seed command (or created alongside the database with. This file should contain all the record creation needed to seed the database with its default values.
The data can then be loaded with the rails db:seed command (or created alongside the database with. This file should contain all the record creation needed to seed the database with its default values.
The data can then be loaded with the rails db:seed command (or created alongside the database with. This file should contain all the record creation needed to seed the database with its default values.
The data can then be loaded with the rails db:seed command (or created alongside the database with. This file should contain all the record creation needed to seed the database with its default values.
The data can then be loaded with the rails db:seed command (or created alongside the database with. This file should contain all the record creation needed to seed the database with its default values.
The data can then be loaded with the rails db:seed command (or created alongside the database with. s',
  user: user)
end

# 6.times do |i|
#   Money.create!(
#     title: "Money #{i}",
#     code: "BTC#{i}",
#     currency: "RUB",
#     price: 199.412424,
#     is_crypto: rand(0..3),
#     website: 'http://bitcoin.org/',
#     price_update: Date.new,
#     body: 'The data can then be loaded with the rails db:seed command The data can then be loaded with the rails db:seed command ',
#   )
# end

12.times do |i|
  Market.create!(
    title: "Market №#{i}",
    website: 'http://rabadan.ru',
    language: 'ru, en',
    body: 'The data can then be loaded with the rails db:seed command The data can then be loaded with the rails db:seed command ',
    twitter: 'rabadan'
  )
end

12.times do |i|
  Exchange.create!(
      title: "Обменик №#{i}",
      website: 'http://rabadan.ru',
      body: 'The data can then be loaded with the rails db:seed command The data can then be loaded with the rails db:seed command ',
      status: 1,
      course_link: 'https://kassa.cc/valuta.xml',
      parser_type: 'default',
  )
end

# 64.times do |i|
#   ExchangeCourse.create!(
#     exchange: Exchange.order('RANDOM()').first,
#     money_from: Money.order('RANDOM()').first,
#     money_to: Money.order('RANDOM()').first,
#     amount_min: rand(1..11),
#     amount_max: rand(10..120),
#     amount_exist: rand(10..92),
#     value_in: rand(1..120),
#     value_out: rand(1..120)
#   )
# end
