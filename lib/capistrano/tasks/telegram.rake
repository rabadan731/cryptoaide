namespace :telegram do
  desc "Start telegram bot"
  task :start do
    on roles(:app) do
      within current_path do
        with rails_env: fetch(:rails_env) do
          execute "ps ax -A | grep 'apps/cryptoaide.*telegram' | awk '{print $1}' | xargs kill"
          execute(:rake, "telegram:bot:poller > /dev/null 2>&1 &")
        end
      end
    end
  end

  desc "Stop telegram bot"
  task :kill do
    on roles(:app) do
      execute "ps ax -A | grep 'apps/cryptoaide.*telegram' | awk '{print $1}' | xargs kill"
    end
  end
end
