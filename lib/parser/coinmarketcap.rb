require 'open-uri'
require 'rexml/document'

class CoinMarketCap
  API_TICKET = 'https://api.coinmarketcap.com/v1/ticker/'
  API_GLOBAL = 'https://api.coinmarketcap.com/v1/global/'

  def self.global
    JSON.parse(open(API_GLOBAL).read)
  end

  def self.currency(code)
    Rails.cache.fetch("coinmarketcap/currency/#{code}", expires_in: 3.minutes) do
      path = "#{API_TICKET}#{code}/"

      JSON.parse(open(path).read)[0]
    end
  end

  def self.all_currency(limit = 5)
    json = JSON.parse(open("#{API_TICKET}?limit=#{limit}").read)
    json.each do |money|
      next if money['symbol'].nil?
      money_from = Money.find_by_symbol_or_create(money['symbol'])
      money_from.price_usd = money['price_usd'] if money['price_usd'].present?
      money_from.price_btc = money['price_btc'] if money['price_btc'].present?
      money_from.last_updated = money['last_updated'] if money['last_updated'].present?
      money_from.day_volume_usd = money['24_volume_usd'] if money['24_volume_usd'].present?
      money_from.market_cap_usd = money['market_cap_usd'] if money['market_cap_usd'].present?
      money_from.available_supply = money['available_supply'] if money['available_supply'].present?
      money_from.total_supply = money['total_supply'] if money['total_supply'].present?
      money_from.max_supply = money['max_supply'] if money['max_supply'].present?
      money_from.percent_change_1h = money['percent_change_1h'] if money['percent_change_1h'].present?
      money_from.percent_change_24h = money['percent_change_24h'] if money['percent_change_24h'].present?
      money_from.percent_change_7d = money['percent_change_7d'] if money['percent_change_7d'].present?
      money_from.save(validate: false)
    end
  end
end