require 'open-uri'
require 'rexml/document'

class Course
  def initialize(exchange)
    @exchange = exchange
  end

  def parse
    case @exchange.parser_type
      when 'test'
        return parsing_test
      when 'default1000'
        return parsing_default(0.001)
      else
        return parsing_default
    end
  end

  def load_xml
    return_data = nil
    begin
      xml_file = open(@exchange.course_link)
      return_data = Nokogiri::XML.parse(xml_file)
    rescue OpenURI::HTTPError => ex
      @exchange.error_count = @exchange.error_count.to_i + 1
      @exchange.error_text = "#{@exchange.error_text}<br />\n #{ex.inspect}"
      @exchange.status = 0 if @exchange.error_count > 10
      @exchange.save
    rescue SocketError => ex
      @exchange.error_count = @exchange.error_count.to_i + 1
      @exchange.error_text = "#{@exchange.error_text}<br />\n #{ex.inspect}"
      @exchange.status = 0 if @exchange.error_count > 10
      @exchange.save
    rescue => ex
      @exchange.error_count = @exchange.error_count.to_i + 1
      @exchange.error_text = "#{@exchange.error_text}<br />\n #{ex.inspect}"
      @exchange.status = 0 if @exchange.error_count > 10
      @exchange.save
    end
    return_data
  end

  def parsing_test
    'test'
  end

  def parsing_default(coefficient = 1)

    moneys = Money.select('id, symbol, currency').all

    document = load_xml

    if document.present?
      exchange_course_item = []

      document.xpath('//rates/item').each do |item|
        money_from_symbol = item.xpath('from').text
        money_to_symbol = item.xpath('to').text
        value_in =  item.xpath('in').text.gsub(/[[:alpha:]]/, '').strip.tr(',','.').to_f
        value_out = item.xpath('out').text.gsub(/[[:alpha:]]/, '').strip.tr(',','.').to_f
        amount = item.xpath('amount').text.gsub(/[[:alpha:]]/, '').strip.tr(',','.').to_f
        minamount = item.xpath('minamount').text.gsub(/[[:alpha:]]/, '').strip.tr(',','.').to_f
        maxamount = item.xpath('maxamount').text.gsub(/[[:alpha:]]/, '').strip.tr(',','.').to_f
        param = item.xpath('param').text

        amount = 0 if amount.blank?
        minamount = 0 if minamount.blank?
        maxamount = 0 if maxamount.blank?
        param = '' if param.blank?

        if money_from_symbol.blank? ||
          money_to_symbol.blank? ||
          value_in.blank? ||
          value_out.blank? ||
          value_in < 0 ||
          value_out < 0
          next
        end

        money_from = moneys.select{|i| i.symbol == money_from_symbol}[0]
        money_from = Money.find_by_symbol_or_create(money_from_symbol) if money_from.nil?

        money_to = moneys.select{|i| i.symbol == money_to_symbol}[0]
        money_to = Money.find_by_symbol_or_create(money_to_symbol) if money_to.nil?

        if money_from.id.present? &&
          money_to.id.present? &&
          money_from.currency.present? &&
          money_to.currency.present?
        end


        exchange_course_item << {
          exchange_id: @exchange.id,
          money_from_id: money_from.id,
          money_to_id: money_to.id,
          currency_from: money_from.currency,
          currency_to: money_to.currency,
          amount_min: minamount,
          amount_max: maxamount,
          amount_exist: amount,
          value_in: (coefficient * value_in),
          value_out: (coefficient * value_out),
          params: param
        }
      end
      ExchangeCourse.where(exchange_id: @exchange.id).delete_all
      ExchangeCourse.import(exchange_course_item, :validate => false)
      # exchange_course_item.each {|i| ExchangeCourse.create!(i) }
    end
  end
end