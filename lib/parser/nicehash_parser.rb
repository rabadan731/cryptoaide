require 'open-uri'
require 'nokogiri'

class NicehashParser
  URL_PROFITABLE = 'https://www.nicehash.com/profitability-calculator/'

  def self.parse_speeds(name)
    sleep 5
    html = open("#{URL_PROFITABLE}#{name}")
    doc = Nokogiri::HTML(html)
    # puts '============================================'
    title = doc.search('div#profitability h2').first.content
    result = {
        title: title,
        data: {}
    }
    doc.search('div.algorithms a').each do |item|
      algorithm_name = item.children[1].content
      algorithm = NicehashParser.find_algorithm(algorithm_name)
      speed = item.children[3].content.split(' ').first
      if algorithm.present? && speed.present? && speed.to_f > 0
        # puts "#{title} = #{algorithm.name} = #{speed}"
        # puts '-------------------------------------------------------'
        result[:data][algorithm] = speed
      end
    end
    # puts '============================================'
    result
  end

  def miners
    mine = {
      '6' => {
          'INTEL' => {
            "intel-cpu-q9450-266ghz" => "",
            "intel-cpu-g1840-280ghz" => "",
            "intel-cpu-g2030-300ghz" => "",
            "intel-cpu-i7-3630qm-240ghz" => "",
            "intel-cpu-i7-6700-340ghz" => "",
            "intel-cpu-i7-7700k-470ghz" => "",
            "intel-cpu-i7-6700t-280ghz" => "",
          }, 'AMD' => {
            "AMD-CPU-A6-5400K" => "",
            "amd-cpu-ryzen-7-1700x-400ghz" => ""
          }
      },
      '4' => {
        'NVIDEA' => {
          "NVIDIA-GTX-560-Ti" => "",
          "NVIDIA-GTX-650-Ti" => "",
          "nvidia-gtx-750-ti" => "",
          "nvidia-geforce-930mx" => "",
          "nvidia-gtx-960" => "",
          "nvidia-gtx-970" => "",
          "nvidia-gtx-980" => "",
          "nvidia-gtx-980-ti" => "",
          "nvidia-gtx-1050-ti" => "",
          "nvidia-gtx-1050" => "",
          "nvidia-gtx-1060-6gb" => "",
          "nvidia-gtx-1070" => "",
          "nvidia-gtx-1080-ti" => "",
          "nvidia-gtx-1080" => "",
          "nvidia-gtx-1070-ti" => "",
          "nvidia-p106-100" => "",
          "nvidia-p104-100" => "",
          "nvidia-gtx-1060-3gb" => "",
          "nvidia-titan-v" => ""
        },
        'AMD' => {
          "amd-hd-7870" => "",
          "amd-hd-7950" => "",
          "amd-r7-360" => "",
          "amd-r7-370" => "",
          "amd-r9-280x" => "",
          "amd-r9-290x" => "",
          "amd-r9-380" => "",
          "amd-r9-380x" => "",
          "amd-r9-390" => "",
          "amd-r9-fury-nano" => "",
          "amd-rx-460-4gb" => "",
          "amd-rx-470-4gb" => "",
          "amd-rx-480-8gb" => "",
          "amd-rx-550-4gb" => "",
          "amd-rx-570-4gb" => "",
          "amd-rx-580-4gb" => "",
          "amd-rx-580-8gb" => "",
          "amd-rx-vega-56" => "",
          "amd-rx-vega-64" => "",
          "amd-vega-frontier-edition" => "",
        }
      },
      '2' => {
        'BITNAIN' => {
          "bitmain-antminer-u3" => "",
          "bitmain-antminer-s3" => "",
          "bitmain-antminer-s4" => "",
          "bitmain-antminer-s5" => "",
          "bitmain-antminer-s7" => "",
          "bitmain-antminer-s7ln" => "",
          "bitmain-antminer-s9" => "",
          "bitmain-antminer-r4" => "",
          "bitmain-antminer-l3" => "",
          "bitmain-antminer-d3" => "",
        },
        'BAIKAL' => {
          "baikal-x11-asic-mini-miner" => "",
          "baikal-x11-x13-x14-x15-qbit-quark-asic-mini-miner" => "",
          "baikal-giant" => "",
          "baikal-giant-x10" => "",
          "baikal-giant-b" => ""
        },
        'innosilicon' => {
            "innosilicon-a2-terminator" => "",
            "innosilicon-a4-dominator" => "",
            "innosilicon-a5-dashmaster-normal-mode" => "",
            "innosilicon-a5-dashmaster-overclock-mode" => "",
        },
        'IBELINK' => {
          "ibelink-dm384m-x11" => "",
          "ibelink-dm22g-x11" => "",
        },
        'CANAAN' => {
          "canaan-creative-avalon-7" => "",
          "canaan-creative-avalon-6" => "",
        },
        'gridseed' => {
          "gridseed-blade-25mhz" => "",
        },
        'gekkoscience' => {
          "gekkoscience-2pac-2x-bm1384-usb-stick-miner" => "",
        },
        'lketc' => {
          "lketc-dragon-miner-1ths" => "",
        },
        'PINIDEA' => {"x11-miner-450m-pinidea-dr2" => "",},
      }
    }

    mine.each do |type_miner|
      type_miner.first #miner_type
      type_miner.last.each do |brand|
        brand.first #brand
        brand.last.each do |item|
          puts "#{type_miner.first} = #{brand.first} = #{item.first}"
          nice = NicehashParser.parse_speeds(item.first)
          miner = Miner.find_or_create_by(
              brand_name: brand.first,
              name: nice[:title],
              miner_type: type_miner.first
          )
          nice[:data].each do |algo|
            MinerSpeed.find_or_create_by(
              miner: miner,
              algorithm: algo.first,
              hash_rate: algo.last.to_f*100
            )
          end
        end
      end
    end

  end


  def self.find_algorithm(name)
    nicehash_list = {
      'Scrypt' => 'scrypt',
      'SHA256' => 'sha-256',
      'ScryptNf' => 'scrypt-n',
      'X11' => 'x11',
      'X13' => 'x13',
      'Keccak' => 'keccak',
      'X15' => '',
      'Nist5' => 'nist5',
      'NeoScrypt' => 'neoscrypt',
      'Lyra2RE' => 'lyra2re',
      'Qubit' => 'qubit',
      'Quark' => 'quark',
      'Lyra2REv2' => 'lyra2rev2',
      'Blake256r14' => 'blake-14r',
      'DaggerHashimoto' => 'ethash',
      'CryptoNight' => 'cryptonight',
      'Lbry' => 'lbry',
      'Equihash' => 'equihash',
      'Pascal' => 'pascal',
      'X11Gost' => 'x11gost',
    }

    if nicehash_list[name].present?
      Algorithm.find_by_slug(nicehash_list[name])
    else
      nil
    end
  end
end