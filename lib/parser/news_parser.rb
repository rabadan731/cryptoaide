require 'open-uri'
require 'rss'

class NewsParser
  def self.load_all_news
    news_source = NewsSource.active.all

    news_source.each do |source|
      self.parsing_rss(source.source, source.website)
    end
  end

  def self.parsing_rss(source, link_parse)
    rss = RSS::Parser.parse(open(link_parse).read, false).items[0..7]

    rss.each do |result|
      news = News.find_by(link: result.link)
      unless news.present?
        news = News.create!(
          title: result.title,
          created_at: result.pubDate,
          link: result.link,
          body: result.description,
          user_id: 1,
          source: source
        )

        tags = result
                 .categories
                 .map(&:content)
                 .map{|i| i.delete('#') }
                 .join(', ')
                 .downcase

        news.tag_list.add(tags, parse: true)
        news.save!
      end
    end

    nil
  end
end