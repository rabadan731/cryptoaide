require 'open-uri'
require 'rexml/document'

class WhattomineParse
  API_CALCULATORS = 'https://whattomine.com/calculators.json'
  API_COINS = 'https://whattomine.com/coins.json'
  API_ASIC = 'https://whattomine.com/asic.json'
  ALGORITHM = ['ethash', 'groestl', 'x11', 'cryptonight', 'equihash', 'lyra2rev2', 'neoscrypt', 'lbry', 'blake-2b', 'blake-14r', 'pascal', 'skunkhash', 'nist5']


  def self.import_data
    json = JSON.parse(open(API_CALCULATORS).read)
    json.first.last.each do |item|
      # next if item.last['status'] != 'Active'
      # puts item.last['algorithm']
      algorithm = Algorithm.find_or_create_by(name: item.last['algorithm'])
      money = Money.find_by_symbol(item.last['tag'])
      if money.nil?
        money = Money.new
        money.currency = item.last['tag']
        money.money_type_id = 1
        money.symbol = item.last['tag']
        money.name = item.first
        money.price_usd = 0
        money.price_btc = 0
        money.day_volume_usd = 0
        money.market_cap_usd = 0
        money.available_supply = 0
        money.total_supply = 0
        money.max_supply = 0
        money.percent_change_1h = 0
        money.percent_change_24h = 0
        money.percent_change_7d = 0
        money.save
      end
      money.whattomine_id = item.last['id']
      money.save

      AlgorithmMoney.find_or_create_by(algorithm: algorithm, money: money)
    end

    nil
  end

  def initialize
    @link = API_COINS
    @params = []
  end

  def get_profit
    json = JSON.parse(open(link).read)
    json.first.last
  end

  def get_average_profit(count_profit = 3)
    profit_items = []
    get_profit.first(count_profit).each do |value|
      profit_items << value.last['btc_revenue24'].to_f
    end

    profit_items.count > 0 ? (profit_items.sum / profit_items.count) : 0
  end

  def link
    "#{@link}?#{@params.to_a.join('&')}"
  end

  def cost(cost)
    @params << "&factor[cost]=#{cost}"
    self
  end

  def self.algorithm_exist?(algorithm)
    ALGORITHM.include?(algorithm)
  end

  def set_algorithm(algorithm, speed, power)
    if WhattomineParse.algorithm_exist?(algorithm)
      param = case algorithm
        when 'ethash'
          "eth=true&factor[eth_hr]=#{speed}&factor[eth_p]=#{power}"
        when 'groestl'
          "grof=true&factor[gro_hr]=#{speed}&factor[gro_p]=#{power}"
        when 'x11'
          "x11gf=true&factor[x11g_hr]=#{speed}&factor[x11g_p]=#{power}"
        when 'cryptonight'
          "cn=true&factor[cn_hr]=#{speed}&factor[cn_p]=#{power}"
        when 'equihash'
          "eq=true&factor[eq_hr]=#{speed}&factor[eq_p]=#{power}"
        when 'lyra2rev2'
          "lre=true&factor[lrev2_hr]=#{speed}&factor[lrev2_p]=#{power}"
        when 'neoscrypt'
          "ns=true&factor[ns_hr]=#{speed}&factor[ns_p]=#{power}"
        when 'lbry'
          "lbry=true&factor[lbry_hr]=#{speed}&factor[lbry_p]=#{power}"
        when 'blake-2b'
          "bk2bf=true&factor[bk2b_hr]=#{speed}&factor[bk2b_p]=#{power}"
        when 'blake-14r'
          "bk14=true&factor[bk14_hr]=#{speed}&factor[bk14_p]=#{power}"
        when 'pascal'
          "pas=true&factor[pas_hr]=#{speed}&factor[pas_p]=#{power}"
        when 'skunkhash'
          "skh=true&factor[skh_hr]=#{speed}&factor[skh_p]=#{power}"
        when 'nist5'
          "n5=true&factor[n5_hr]=#{speed}&factor[n5_p]=#{power}"
        else
          nil
        end
    end
    @params << param unless param.nil?
    self
  end
end