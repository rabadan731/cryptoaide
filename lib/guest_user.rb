require 'rails-i18n'
class GuestUser
  def name
    'Гость'
  end

  def email
    'guest@cryptoaide.net'
  end

  def avatar
    '/assets/a1.jpg'
  end

  def id
    0
  end
end