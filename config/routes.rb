Rails.application.routes.draw do
  # telegram_webhooks TelegramWebhooksController

  # пользователи - регистрация
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks'
  }
  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup
  get 'users/:id/show', to: 'users#show', as: :user_show
  get 'users/:id/audit', to: 'users#audit', as: :user_audit

  # Визуальный редактор
  mount Ckeditor::Engine => '/ckeditor'
  # Коменатрии
  mount Commontator::Engine => '/commontator'

  # Страницы сайта
  resources :pages
  root "page#index"

  # Биржы криптовалют
  resources :markets, param: :slug do
    get 'go'
  end

  # Новости
  resources :news, param: :slug do
    get 'go', as: :go
  end


  get 'money/top/:type', to: 'money#top',  as: :money_top
  get 'money/:slug/mining', to: 'money#mining', as: :mining_money
  get 'money/:slug/events', to: 'money#events', as: :events_money
  get 'money/:slug/news', to: 'money#news', as: :news_money

  # Криптовалюты
  resources :money, param: :slug do
    resources :explorers, param: :explorer_slug
    resources :events, param: :event_slug
  end



  # Обменики криптовалют
  resources :exchanges, param: :slug do
    get 'go', param: :slug, as: :go_exchange
    get 'load_course', param: :slug
  end

  get 'change', to: 'change#change', as: :chenge_index
  get 'change/:currency_from/:money_from/:currency_to/:money_to', to: 'change#change', as: :chenge

  # Статьи / категории
  resources :articles, param: :slug
  resources :article_categories, param: :slug

  # Теги объектов
  get 'tags/:tag', to: 'articles#index', as: :tag

  # Пулы для майнинга
  resources :pools, param: :slug do
    get 'go'
    resources :moneys, controller: :pool_moneys do
      get 'go', as: :go
    end
  end

  # Оборудование для майнинга
  post 'miners/import', as: :import_miners, to: 'miners#import'
  get 'miners/import_nicehash', as: :import_nicehash_miners, to: 'miners#import_nicehash'
  resources :miners, param: :slug do
    resources :speeds, controller: :miner_speeds
    resources :modes, controller: :miner_modes, param: :slug
  end

  get 'algorithms/import', to: 'algorithms#import',  as: :algorithms_import
  resources :algorithms, param: :slug

  resources :socials, param: :slug do
    resources :youtube, controller: :youtube_chanels, param: :slug
    get 'youtube/:slug/youtube_update', to: 'youtube_chanels#update_from_youtube', as: :youtube_update
    get 'youtube/:slug/go', to: 'youtube_chanels#go', as: :go_youtube
    # get 'youtube/:slug', to: 'youtube_chanels#index', as: :youtube_chanels

    resources :telegram, controller: :telegram_chanels, param: :slug
    get 'telegram/:slug/go', to: 'telegram_chanels#go', as: :go_telegram
  end

  resources :go_counters

  resources :wallets, param: :slug do
    get 'go'
  end

  resources :news_sources

  authenticate :user, ->(u) { u.admin? } do
    require 'sidekiq/web'
    mount Sidekiq::Web => '/sidekiq'
  end
end
