require 'rufus-scheduler'
require 'parser/course'

scheduler = Rufus::Scheduler::singleton

scheduler.every '1d' do
  rake "-s sitemap:refresh"
end

scheduler.every '5m' do
  SystemService.restart_workers
end
