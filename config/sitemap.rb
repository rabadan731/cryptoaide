# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://cryptoaide.net"

SitemapGenerator::Sitemap.create do

  add news_index_path
  News.published.each do |news|
    add news_path(news)
  end

  add money_index_path
  Money.all.each do |money|
    add money_path(money)
  end

  add pools_path
  Pool.all.each do |pool|
    add pool_path(pool)
    pool.pool_moneys.each do |pool_money|
      add pool_money_path(pool, pool_money)
    end
  end

  add miners_path
  Miner.all.each do |miner|
    add miner_path(miner)
    miner.miner_speeds.each do |miner_speed|
      add miner_speed_path(miner, miner_speed)
    end
  end

  add chenge_index_path
  Exchange.all.each do |exchange|
    add exchange_path(exchange)
  end

  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end
