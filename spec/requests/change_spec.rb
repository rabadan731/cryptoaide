require 'rails_helper'

RSpec.describe "Change", type: :request do
  before(:each) do
    FactoryBot.create(:money, symbol: 'ETH', currency: 'ETH', money_type_id: 1)
    FactoryBot.create(:money, symbol: 'BTC', currency: 'BTC', money_type_id: 1)
    FactoryBot.create(:money, symbol: 'SBRUB', currency: 'RUB', money_type_id: 2)
    FactoryBot.create(:money, symbol: 'WMUSD', currency: 'USD', money_type_id: 2)
  end

  describe "GOOD" do
    it "ALL ALL ALL ALL" do
      get chenge_path('ALL','ALL','ALL','ALL')
      expect(response).to have_http_status(200)
    end
    it "RUB ALL ALL ALL" do
      get chenge_path('ALL','ALL','ALL','ALL')
      expect(response).to have_http_status(200)
    end
    it "RUB ALL CRYPTO ALL" do
      get chenge_path('RUB','ALL','CRYPTO','ALL')
      expect(response).to have_http_status(200)
    end
    it "RUB SBRUB CRYPTO ALL" do
      get chenge_path('RUB','SBRUB','CRYPTO','ALL')
      expect(response).to have_http_status(200)
    end
    it "RUB SBRUB CRYPTO BTC" do
      get chenge_path('RUB','SBRUB','CRYPTO','BTC')
      expect(response).to have_http_status(200)
    end
    it "RUB SBRUB CRYPTO ALL" do
      get chenge_path('RUB','SBRUB','CRYPTO','ALL')
      expect(response).to have_http_status(200)
    end
    it "CRYPTO ALL RUB ALL" do
      get chenge_path('CRYPTO','ALL','RUB','ALL')
      expect(response).to have_http_status(200)
    end
    it "CRYPTO BTC RUB ALL" do
      get chenge_path('CRYPTO','BTC','RUB','ALL')
      expect(response).to have_http_status(200)
    end
    it "ALL BTC RUB ALL" do
      get chenge_path('ALL','BTC','RUB','ALL')
      expect(response).to have_http_status(200)
    end
    it "ALL BTC ALL SBRUB" do
      get chenge_path('ALL','BTC','ALL','SBRUB')
      expect(response).to have_http_status(200)
    end
    it "ALL BTC RUB SBRUB" do
      get chenge_path('ALL','BTC','RUB','SBRUB')
      expect(response).to have_http_status(200)
    end
    it "CRYPTO BTC RUB SBRUB" do
      get chenge_path('CRYPTO','BTC','RUB','SBRUB')
      expect(response).to have_http_status(200)
    end
  end


  describe "BAD" do
    context "currency_from" do
      it "unikom currency" do
        get chenge_path('BLABLA','ETH','ALL','ALL')
        expect(response).to have_http_status(301)
        expect(response).to redirect_to(chenge_path('CRYPTO','ETH','ALL','ALL'))
      end
      it "others currency" do
        get chenge_path('RUB','ETH','ALL','ALL')
        expect(response).to have_http_status(301)
        expect(response).to redirect_to(chenge_path('CRYPTO','ETH','ALL','ALL'))
      end
    end

    context "currency_to" do
      it "unikom currency" do
        get chenge_path('ALL','ALL','BLABLA','ETH')
        expect(response).to have_http_status(301)
        expect(response).to redirect_to(chenge_path('ALL','ALL','CRYPTO','ETH'))
      end
      it "others currency" do
        get chenge_path('ALL','ALL','RUB','ETH')
        expect(response).to have_http_status(301)
        expect(response).to redirect_to(chenge_path('ALL','ALL','CRYPTO','ETH'))
      end
      it "others currency" do
        get chenge_path('CRYPTO','BTC','USD','SBRUB')
        expect(response).to have_http_status(301)
        expect(response).to redirect_to(chenge_path('CRYPTO','BTC','RUB','SBRUB'))
      end
    end
  end
end
