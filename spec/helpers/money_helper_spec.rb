require 'rails_helper'

RSpec.describe MoneyHelper, type: :helper do
  describe MoneyHelper do
    describe "nice price" do
      it "print nice number" do
        expect(helper.nice_price('10000.11111119', 0)).to eq("10 000 ")
        expect(helper.nice_price('10000.11111119', 2)).to eq("10 000.11 ")
        expect(helper.nice_price('10000.11111119', 4)).to eq("10 000.1111 ")
        expect(helper.nice_price('10000.11111119', 8)).to eq("10 000.11111119 ")
      end
    end
  end
end
