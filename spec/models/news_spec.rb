require 'rails_helper'

RSpec.describe News, type: :model do
  let(:news) { News.create(title: 'MyString Новости 5 * -') }
  context '.set_slug' do
    it 'correct crate slug' do
      expect(news.slug).to eq 'mystring-nowosti-5'
    end
  end
end
