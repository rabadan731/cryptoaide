require 'rails_helper'

RSpec.describe Explorer, type: :model do
  let(:explorer) { FactoryBot.create(:explorer, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(explorer.slug).to eq 'title-2'
    end
  end
end
