require 'rails_helper'

RSpec.describe Wallet, type: :model do
  let(:wallet) { FactoryBot.create(:wallet, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(wallet.slug).to eq 'title-2'
    end
  end
end
