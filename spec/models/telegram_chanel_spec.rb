require 'rails_helper'

RSpec.describe TelegramChanel, type: :model do
  let(:telegram_chanel) { FactoryBot.create(:telegram_chanel, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(telegram_chanel.slug).to eq 'title-2'
    end
  end
end
