require 'rails_helper'

RSpec.describe ArticleCategory, type: :model do
  let(:article_category) { FactoryBot.create(:article_category, title: 'TItle 6')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(article_category.slug).to eq 'title-6'
    end
  end
end
