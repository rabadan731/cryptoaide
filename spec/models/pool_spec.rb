require 'rails_helper'

RSpec.describe Pool, type: :model do
  let(:pool) { FactoryBot.create(:pool, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(pool.slug).to eq 'title-2'
    end
  end
end
