require 'rails_helper'

RSpec.describe Exchange, type: :model do
  let(:exchange) { FactoryBot.create(:exchange, title: 'Обменик 5')}

  context '.set_slug' do
    it 'correct crate slug' do
      expect(exchange.slug).to eq 'obmenik-5'
    end
  end
end
