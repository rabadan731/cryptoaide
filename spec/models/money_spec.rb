require 'rails_helper'

RSpec.describe Money, type: :model do

  let(:money) { FactoryBot.create(:money, symbol: 'TesTs')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(money.slug).to eq 'tests'
    end
  end

  describe '.is_crypto?' do
    let(:money_fiat) { FactoryBot.create(:money, money_type_id: 3, symbol: 'CODs')}
    it 'correct crate slug' do
      expect(money.is_crypto?).to be_truthy
      expect(money_fiat.is_crypto?).to be_falsey
    end
  end

  describe '#find_by_symbol_or_create' do
    it 'no create dublicate' do
      money_symbol = money.symbol
      expect{Money.find_by_symbol_or_create(money_symbol)}.to change{ Money.count }.by(0)
      expect{Money.find_by_symbol_or_create("NEWTESTMONEY")}.to change{ Money.count }.by(1)
    end
  end
end
