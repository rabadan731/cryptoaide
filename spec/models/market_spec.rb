require 'rails_helper'

RSpec.describe Market, type: :model do
  let(:market) { FactoryBot.create(:market, title: 'tEST TESt slug *()1')}

  context '.set_slug' do
    it 'correct crate slug' do
      expect(market.slug).to eq 'test-test-slug-1'
    end
  end
end
