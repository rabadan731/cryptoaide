require 'rails_helper'

RSpec.describe YoutubeChanel, type: :model do
  let(:youtube_chanel) { FactoryBot.create(:youtube_chanel, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(youtube_chanel.slug).to eq 'title-2'
    end
  end
end
