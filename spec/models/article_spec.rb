require 'rails_helper'

RSpec.describe Article, type: :model do
  let(:article) { FactoryBot.create(:article, title: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(article.slug).to eq 'title-2'
    end
  end
end
