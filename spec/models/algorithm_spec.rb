require 'rails_helper'

RSpec.describe Algorithm, type: :model do
  let(:algorithm) { FactoryBot.create(:algorithm, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(algorithm.slug).to eq 'title-2'
    end
  end
end
