require 'rails_helper'

RSpec.describe Event, type: :model do
  let(:event) { FactoryBot.create(:event, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(event.slug).to eq 'title-2'
    end
  end
end
