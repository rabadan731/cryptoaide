require 'rails_helper'

RSpec.describe Social, type: :model do
  let(:social) { FactoryBot.create(:social, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(social.slug).to eq 'title-2'
    end
  end
end
