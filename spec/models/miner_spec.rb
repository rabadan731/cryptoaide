require 'rails_helper'

RSpec.describe Miner, type: :model do
  let(:miner) { FactoryBot.create(:miner, name: 'TItle 2')}

  describe '.set_slug' do
    it 'correct crate slug' do
      expect(miner.slug).to eq 'title-2'
    end
  end
end
