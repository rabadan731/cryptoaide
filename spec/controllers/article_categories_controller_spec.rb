require 'rails_helper'

RSpec.describe ArticleCategoriesController, type: :controller do

  let(:article_category) { FactoryBot.create(:article_category) }
  let(:user) { FactoryBot.create(:user) }

  let(:valid_attributes) { { title: 'Category test' } }
  let(:invalid_attributes) { { title: nil } }
  let(:valid_session) { {} }

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: article_category.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new ArticleCategory' do
            expect {
              post :create, params: valid_attributes, session: valid_session
            }.to change(ArticleCategory, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {article_category: {title: 'title', code: 'TESYI'} }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new ArticleCategory" do
            expect {
              post :create, params: {ArticleCategory: valid_attributes}, session: valid_session
            }.to change(ArticleCategory, :count).by(0)
          end

          it "redirects to the created ArticleCategory" do
            post :create, params: {ArticleCategory: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) { { title: 'ttiittllee' } }

          it "updates the requested ArticleCategory" do
            old_title = article_category.title
            put :update, params: {slug: article_category.to_param, ArticleCategory: new_attributes}, session: valid_session
            article_category.reload
            expect(article_category.title).to eq old_title
          end

          it "redirects to the ArticleCategory" do
            put :update, params: {slug: article_category.to_param, ArticleCategory: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested ArticleCategory" do
          article_category
          expect {
            delete :destroy, params: {slug: article_category.to_param}, session: valid_session
          }.to change(ArticleCategory, :count).by(0)
        end

        it "redirects to the ArticleCategory list" do
          delete :destroy, params: {slug: article_category.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) { sign_in user }

    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {slug: article_category.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new ArticleCategory" do
          expect {
            post :create, params: {article_category: valid_attributes}, session: valid_session
          }.to change(ArticleCategory, :count).by(1)
        end

        it "redirects to the created article_category" do
          post :create, params: {article_category: valid_attributes}, session: valid_session
          expect(response).to redirect_to(ArticleCategory.last)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {article_category: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {{ title: 'New title'}}

        it "updates the requested article_category" do
          put :update, params: {
            slug: article_category.to_param,
            article_category: new_attributes
          }, session: valid_session
          article_category.reload
          expect(article_category.title).to eq(new_attributes[:title])
        end

        it "redirects to the article_category" do
          put :update, params: {
            slug: article_category.to_param,
            article_category: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(article_category)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'edit' template)" do
          put :update, params: {
            slug: article_category.to_param,
            article_category: invalid_attributes
          }, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested article_category" do
        article_category
        expect {
          delete :destroy, params: {slug: article_category.to_param}, session: valid_session
        }.to change(ArticleCategory, :count).by(-1)
      end

      it "redirects to the article_categories list" do
        delete :destroy, params: {slug: article_category.to_param}, session: valid_session
        expect(response).to redirect_to(article_categories_url)
      end
    end
  end
end
