require 'rails_helper'

RSpec.describe SocialsController, type: :controller do

  let!(:social) {FactoryBot.create(:social)}
  let(:valid_attributes) {{name: 'BTC test'}}
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: social.to_param}, session: valid_session
        expect(response).to be_success
      end
    end
    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Social' do
            expect {
              post :create, params: {social: valid_attributes}, session: valid_session
            }.to change(Social, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {social: {name: 'name', code: 'TESYI'}}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Social" do
            expect {
              post :create, params: {Social: valid_attributes}, session: valid_session
            }.to change(Social, :count).by(0)
          end

          it "redirects to the created Social" do
            post :create, params: {Social: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested Social" do
            old_name = social.name
            put :update, params: {slug: social.to_param, Social: new_attributes}, session: valid_session
            social.reload
            expect(social.name).to eq old_name
          end

          it "redirects to the Social" do
            put :update, params: {slug: social.to_param, Social: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Social" do
          expect {
            delete :destroy, params: {slug: social.to_param}, session: valid_session
          }.to change(Social, :count).by(0)
        end

        it "redirects to the Social list" do
          delete :destroy, params: {slug: social.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {slug: social.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Social" do
          expect {
            post :create, params: {social: valid_attributes}, session: valid_session
          }.to change(Social, :count).by(1)
        end

        it "redirects to the created social" do
          post :create, params: {social: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Social.last)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
              name: 'Test test'
          }
        }

        it "updates the requested social" do
          put :update, params: {slug: social.to_param, social: new_attributes}, session: valid_session
          social.reload
          expect(social.name).to eq(new_attributes[:name])
        end

        it "redirects to the social" do
          put :update, params: {slug: social.to_param, social: valid_attributes}, session: valid_session
          expect(response).to redirect_to(social)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested social" do
        expect {
          delete :destroy, params: {slug: social.to_param}, session: valid_session
        }.to change(Social, :count).by(-1)
      end

      it "redirects to the pools list" do
        delete :destroy, params: {slug: social.to_param}, session: valid_session
        expect(response).to redirect_to(socials_url)
      end
    end
  end
end