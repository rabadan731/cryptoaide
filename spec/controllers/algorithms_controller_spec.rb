require 'rails_helper'

RSpec.describe AlgorithmsController, type: :controller do

  let!(:algorithm) {FactoryBot.create(:algorithm)}
  let(:valid_attributes) {{name: 'BTC test'}}
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: algorithm.to_param}, session: valid_session
        expect(response).to be_success
      end
    end
    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Algorithm' do
            expect {
              post :create, params: {algorithm: valid_attributes}, session: valid_session
            }.to change(Algorithm, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {algorithm: {name: 'name', code: 'TESYI'}}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Algorithm" do
            expect {
              post :create, params: {Algorithm: valid_attributes}, session: valid_session
            }.to change(Algorithm, :count).by(0)
          end

          it "redirects to the created Algorithm" do
            post :create, params: {Algorithm: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested Algorithm" do
            old_name = algorithm.name
            put :update, params: {slug: algorithm.to_param, Algorithm: new_attributes}, session: valid_session
            algorithm.reload
            expect(algorithm.name).to eq old_name
          end

          it "redirects to the Algorithm" do
            put :update, params: {slug: algorithm.to_param, Algorithm: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Algorithm" do
          expect {
            delete :destroy, params: {slug: algorithm.to_param}, session: valid_session
          }.to change(Algorithm, :count).by(0)
        end

        it "redirects to the Algorithm list" do
          delete :destroy, params: {slug: algorithm.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {slug: algorithm.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Algorithm" do
          expect {
            post :create, params: {algorithm: valid_attributes}, session: valid_session
          }.to change(Algorithm, :count).by(1)
        end

        it "redirects to the created algorithm" do
          post :create, params: {algorithm: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Algorithm.last)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
              name: 'Test test'
          }
        }

        it "updates the requested algorithm" do
          put :update, params: {slug: algorithm.to_param, algorithm: new_attributes}, session: valid_session
          algorithm.reload
          expect(algorithm.name).to eq(new_attributes[:name])
        end

        it "redirects to the algorithm" do
          put :update, params: {slug: algorithm.to_param, algorithm: valid_attributes}, session: valid_session
          expect(response).to redirect_to(algorithm)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested algorithm" do
        expect {
          delete :destroy, params: {slug: algorithm.to_param}, session: valid_session
        }.to change(Algorithm, :count).by(-1)
      end

      it "redirects to the pools list" do
        delete :destroy, params: {slug: algorithm.to_param}, session: valid_session
        expect(response).to redirect_to(algorithms_url)
      end
    end
  end
end