require 'rails_helper'

RSpec.describe YoutubeChanelsController, type: :controller do

  let!(:youtube_chanel) {FactoryBot.create(:youtube_chanel)}
  let!(:social) { FactoryBot.create(:social) }
  let(:valid_attributes) {{name: 'BTC test', chanel_id: 'test'}}
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {
            social_slug: social.slug
        }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {
            social_slug: social.slug,
            slug: youtube_chanel.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end
    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {
              social_slug: social.slug
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new YoutubeChanel' do
            expect {
              post :create, params: {
                  social_slug: social.slug,
                  youtube_chanel: valid_attributes
              }, session: valid_session
            }.to change(YoutubeChanel, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {
                social_slug: social.slug,
                youtube_chanel: {name: 'name', code: 'TESYI'}
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new YoutubeChanel" do
            expect {
              post :create, params: {
                  social_slug: social.slug,
                  YoutubeChanel: valid_attributes
              }, session: valid_session
            }.to change(YoutubeChanel, :count).by(0)
          end

          it "redirects to the created YoutubeChanel" do
            post :create, params: {
                social_slug: social.slug,
                YoutubeChanel: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested YoutubeChanel" do
            old_name = youtube_chanel.name
            put :update, params: {
                social_slug: social.slug,
                slug: youtube_chanel.to_param,
                YoutubeChanel: new_attributes
            }, session: valid_session
            youtube_chanel.reload
            expect(youtube_chanel.name).to eq old_name
          end

          it "redirects to the YoutubeChanel" do
            put :update, params: {
                social_slug: social.slug,
                slug: youtube_chanel.to_param,
                YoutubeChanel: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested YoutubeChanel" do
          expect {
            delete :destroy, params: {
                social_slug: social.slug,
                slug: youtube_chanel.to_param
            }, session: valid_session
          }.to change(YoutubeChanel, :count).by(0)
        end

        it "redirects to the YoutubeChanel list" do
          delete :destroy, params: {
              social_slug: social.slug,
              slug: youtube_chanel.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {
            social_slug: social.slug
        }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {
            social_slug: social.slug,
            slug: youtube_chanel.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new YoutubeChanel" do
          expect {
            post :create, params: {
                social_slug: social.slug,
                youtube_chanel: valid_attributes
            }, session: valid_session
          }.to change(YoutubeChanel, :count).by(1)
        end

        it "redirects to the created youtube_chanel" do
          post :create, params: {
              social_slug: social.slug,
              youtube_chanel: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(social_youtube_path(YoutubeChanel.last.social, YoutubeChanel.last))
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
              name: 'Test test'
          }
        }

        it "updates the requested youtube_chanel" do
          put :update, params: {
              social_slug: social.slug,
              slug: youtube_chanel.to_param,
              youtube_chanel: new_attributes
          }, session: valid_session
          youtube_chanel.reload
          expect(youtube_chanel.name).to eq(new_attributes[:name])
        end

        it "redirects to the youtube_chanel" do
          put :update, params: {
              social_slug: youtube_chanel.social.slug,
              slug: youtube_chanel.to_param,
              youtube_chanel: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(social_youtube_path(youtube_chanel.social, youtube_chanel))
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested youtube_chanel" do
        expect {
          delete :destroy, params: {
              social_slug: social.slug,
              slug: youtube_chanel.to_param
          }, session: valid_session
        }.to change(YoutubeChanel, :count).by(-1)
      end

      it "redirects to the pools list" do
        delete :destroy, params: {
            slug: youtube_chanel.to_param,
            social_slug: social.slug
        }, session: valid_session
        expect(response).to redirect_to(social_path(social))
      end
    end
  end
end