require 'rails_helper'

RSpec.describe PoolMoneysController, type: :controller do

  let!(:pool_money) {FactoryBot.create(:pool_money)}
  let(:valid_attributes) {
    {
      pool_id: pool_money.pool.id,
      money_id: FactoryBot.create(:money).id,
      website: "http://site.xx",
      partner_link: "http://site.xx?2354365"
    }
  }
  let(:invalid_attributes) {{pool_id: 'ggfds', money_id: 'wwww'}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}
  let(:pool_slug) {pool_money.pool.slug}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {
          pool_slug: pool_slug
        }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {
          pool_slug: pool_slug,
          id: pool_money.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe "GET #new" do
        it "returns a success response" do
          get :new, params: {
            pool_slug: pool_slug
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "GET #edit" do
        it "returns a success response" do
          get :edit, params: {
            pool_slug: pool_slug,
            id: pool_money.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new PoolMoney" do
            expect {
              post :create, params: {
                pool_slug: pool_slug,
                PoolMoney: valid_attributes
              }, session: valid_session
            }.to change(PoolMoney, :count).by(0)
          end

          it "redirects to the created PoolMoney" do
            post :create, params: {
              pool_slug: pool_slug,
              PoolMoney: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{hash_rate: 666}}

          it "updates the requested PoolMoney" do
            old_website = pool_money.website
            put :update, params: {
              pool_slug: pool_slug,
              id: pool_money.to_param,
              PoolMoney: new_attributes
            }, session: valid_session
            pool_money.reload
            expect(pool_money.website).to eq old_website
          end

          it "redirects to the PoolMoney" do
            put :update, params: {
              pool_slug: pool_slug,
              id: pool_money.to_param,
              PoolMoney: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested PoolMoney" do
          pool_money
          expect {
            delete :destroy, params: {
              pool_slug: pool_slug,
              id: pool_money.to_param
            }, session: valid_session
          }.to change(PoolMoney, :count).by(0)
        end

        it "redirects to the PoolMoney list" do
          delete :destroy, params: {
            pool_slug: pool_slug,
            id: pool_money.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}


    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {
          pool_slug: pool_slug
        }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {
          pool_slug: pool_slug,
          id: pool_money.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new PoolMoney" do
          expect {
            post :create, params: {
              pool_slug: pool_slug,
              pool_money: valid_attributes
            }, session: valid_session
          }.to change(PoolMoney, :count).by(1)
        end

        it "redirects to the created pool_money" do
          post :create, params: {
            pool_slug: pool_slug,
            pool_money: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(pool_money_path(PoolMoney.last.pool, PoolMoney.last))
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {
            pool_slug: pool_slug, pool_money: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
            website: "http://spec.site"
          }
        }

        it "updates the requested pool_money" do
          put :update, params: {
            pool_slug: pool_slug,
            id: pool_money.to_param,
            pool_money: new_attributes
          }, session: valid_session
          pool_money.reload
          expect(pool_money.website).to eq(new_attributes[:website])
        end

        it "redirects to the pool_money" do
          put :update, params: {
            pool_slug: pool_money.pool.slug,
            id: pool_money.to_param,
            pool_money: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(pool_money_path(pool_money.pool, pool_money))
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'edit' template)" do
          put :update, params: {
            pool_slug: pool_slug,
            id: pool_money.to_param,
            pool_money: invalid_attributes
          }, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested pool_money" do
        expect {
          delete :destroy, params: {
            pool_slug: pool_slug,
            id: pool_money.to_param
          }, session: valid_session
        }.to change(PoolMoney, :count).by(-1)
      end

      it "redirects to the pool_moneys list" do
        delete :destroy, params: {
          pool_slug: pool_slug,
          id: pool_money.to_param
        }, session: valid_session
        expect(response).to redirect_to(pool_url(pool_money.pool))
      end
    end
  end
end