require 'rails_helper'

RSpec.describe ExchangesController, type: :controller do

  let(:exchange) {FactoryBot.create(:exchange)}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_attributes) {{title: "Title title test"}}
  let(:invalid_attributes) {{title: nil}}
  let(:valid_session) {{}}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: exchange.to_param}, session: valid_session
        expect(response).to be_success
      end
    end


    context 'No access' do
      describe "GET #new" do
        it "returns a success response" do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "GET #edit" do
        it "returns a success response" do
          get :edit, params: {slug: exchange.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Exchange" do
            expect {
              post :create, params: {Exchange: valid_attributes}, session: valid_session
            }.to change(Exchange, :count).by(0)
          end

          it "redirects to the created Exchange" do
            post :create, params: {Exchange: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{title: 'ttiittllee'}}

          it "updates the requested Exchange" do
            old_title = exchange.title
            put :update, params: {slug: exchange.to_param, Exchange: new_attributes}, session: valid_session
            exchange.reload
            expect(exchange.title).to eq old_title
          end

          it "redirects to the Exchange" do
            put :update, params: {slug: exchange.to_param, Exchange: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Exchange" do
          exchange
          expect {
            delete :destroy, params: {slug: exchange.to_param}, session: valid_session
          }.to change(Exchange, :count).by(0)
        end

        it "redirects to the Exchange list" do
          delete :destroy, params: {slug: exchange.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end

  end

  context 'Authorized User' do
    before(:each) {sign_in user}


    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        exchange = Exchange.create! valid_attributes
        get :edit, params: {slug: exchange.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Exchange" do
          expect {
            post :create, params: {exchange: valid_attributes}, session: valid_session
          }.to change(Exchange, :count).by(1)
        end

        it "redirects to the created exchange" do
          post :create, params: {exchange: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Exchange.last)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {exchange: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
            title: 'New title'
          }
        }

        it "updates the requested exchange" do
          exchange = Exchange.create! valid_attributes
          put :update, params: {slug: exchange.to_param, exchange: new_attributes}, session: valid_session
          exchange.reload
          expect(exchange.title).to eq(new_attributes[:title])
        end

        it "redirects to the exchange" do
          exchange = Exchange.create! valid_attributes
          put :update, params: {slug: exchange.to_param, exchange: valid_attributes}, session: valid_session
          expect(response).to redirect_to(exchange)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'edit' template)" do
          exchange = Exchange.create! valid_attributes
          put :update, params: {slug: exchange.to_param, exchange: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested exchange" do
        exchange = Exchange.create! valid_attributes
        expect {
          delete :destroy, params: {slug: exchange.to_param}, session: valid_session
        }.to change(Exchange, :count).by(-1)
      end

      it "redirects to the exchanges list" do
        exchange = Exchange.create! valid_attributes
        delete :destroy, params: {slug: exchange.to_param}, session: valid_session
        expect(response).to redirect_to(exchanges_url)
      end
    end
  end
end
