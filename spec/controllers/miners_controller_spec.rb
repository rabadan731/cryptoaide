require 'rails_helper'

RSpec.describe MinersController, type: :controller do

  let!(:miner) { FactoryBot.create(:miner) }
  let(:valid_attributes) { { name: 'BTC test' } }
  let(:invalid_attributes) { {a: 3} }
  let(:user) { FactoryBot.create(:user) }
  let(:valid_session) { {} }

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: miner.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Miner' do
            expect {
              post :create, params: {miner: valid_attributes}, session: valid_session
            }.to change(Miner, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {miner: {name: 'name', code: 'TESYI'} }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Miner" do
            expect {
              post :create, params: {Miner: valid_attributes}, session: valid_session
            }.to change(Miner, :count).by(0)
          end

          it "redirects to the created Miner" do
            post :create, params: {Miner: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) { { name: 'ttiittllee' } }

          it "updates the requested Miner" do
            old_name = miner.name
            put :update, params: {slug: miner.to_param, Miner: new_attributes}, session: valid_session
            miner.reload
            expect(miner.name).to eq old_name
          end

          it "redirects to the Miner" do
            put :update, params: {slug: miner.to_param, Miner: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Miner" do
          miner
          expect {
            delete :destroy, params: {slug: miner.to_param}, session: valid_session
          }.to change(Miner, :count).by(0)
        end

        it "redirects to the Miner list" do
          delete :destroy, params: {slug: miner.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}

    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {slug: miner.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Miner" do
          expect {
            post :create, params: {miner: valid_attributes}, session: valid_session
          }.to change(Miner, :count).by(1)
        end

        it "redirects to the created miner" do
          post :create, params: {miner: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Miner.last)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
            name: 'NEw title t363'
          }
        }

        it "updates the requested miner" do
          put :update, params: {slug: miner.to_param, miner: new_attributes}, session: valid_session
          miner.reload
          expect(miner.name).to eq(new_attributes[:name])
        end

        it "redirects to the miner" do
          put :update, params: {slug: miner.to_param, miner: valid_attributes}, session: valid_session
          expect(response).to redirect_to(miner)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested miner" do
        expect {
          delete :destroy, params: {slug: miner.to_param}, session: valid_session
        }.to change(Miner, :count).by(-1)
      end

      it "redirects to the miners list" do
        delete :destroy, params: {slug: miner.to_param}, session: valid_session
        expect(response).to redirect_to(miners_url)
      end
    end
  end
end
