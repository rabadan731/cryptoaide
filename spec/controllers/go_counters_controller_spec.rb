require 'rails_helper'

RSpec.describe GoCountersController, type: :controller do

  let!(:go_counter) {FactoryBot.create(:go_counter)}
  let(:valid_attributes) {{name: 'BTC test'}}
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}

  context 'Anonymous user' do

    context 'No access' do
      describe "GET #index" do
        it "returns a success response" do
          get :index, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "GET #show" do
        it "returns a success response" do
          get :show, params: {id: go_counter.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new GoCounter' do
            expect {
              post :create, params: {go_counter: valid_attributes}, session: valid_session
            }.to change(GoCounter, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {go_counter: {name: 'name', code: 'TESYI'}}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new GoCounter" do
            expect {
              post :create, params: {GoCounter: valid_attributes}, session: valid_session
            }.to change(GoCounter, :count).by(0)
          end

          it "redirects to the created GoCounter" do
            post :create, params: {GoCounter: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{ip: '1.1.1.1'}}

          it "updates the requested GoCounter" do
            old_ip = go_counter.ip
            put :update, params: {id: go_counter.to_param, GoCounter: new_attributes}, session: valid_session
            go_counter.reload
            expect(go_counter.ip).to eq old_ip
          end

          it "redirects to the GoCounter" do
            put :update, params: {id: go_counter.to_param, GoCounter: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested GoCounter" do
          expect {
            delete :destroy, params: {id: go_counter.to_param}, session: valid_session
          }.to change(GoCounter, :count).by(0)
        end

        it "redirects to the GoCounter list" do
          delete :destroy, params: {id: go_counter.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {id: go_counter.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new GoCounter" do
          expect {
            post :create, params: {go_counter: valid_attributes}, session: valid_session
          }.to change(GoCounter, :count).by(1)
        end

        it "redirects to the created go_counter" do
          post :create, params: {go_counter: valid_attributes}, session: valid_session
          expect(response).to redirect_to(GoCounter.last)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {{ip: '1.1.1.1'}}

        it "updates the requested go_counter" do
          put :update, params: {id: go_counter.to_param, go_counter: new_attributes}, session: valid_session
          go_counter.reload
          expect(go_counter.ip).to eq(new_attributes[:ip])
        end

        it "redirects to the go_counter" do
          put :update, params: {id: go_counter.to_param, go_counter: valid_attributes}, session: valid_session
          expect(response).to redirect_to(go_counter)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested go_counter" do
        expect {
          delete :destroy, params: {id: go_counter.to_param}, session: valid_session
        }.to change(GoCounter, :count).by(-1)
      end

      it "redirects to the pools list" do
        delete :destroy, params: {id: go_counter.to_param}, session: valid_session
        expect(response).to redirect_to(go_counters_url)
      end
    end
  end
end