require 'rails_helper'

RSpec.describe MinerSpeedsController, type: :controller do

  let!(:miner_speed) {FactoryBot.create(:miner_speed)}
  let(:valid_attributes) {
    {
      miner_id: miner_speed.miner.id,
      algorithm_id: FactoryBot.create(:algorithm).id,
      algorithm_dual_id: FactoryBot.create(:algorithm).id,
      hash_rate: 132,
      hash_rate_dual: 342,
      power_rate: 5445,
    }
  }
  let(:invalid_attributes) {
    {
      hash_rate: 'drssdbh',
      hash_rate_dual: 'rahseh',
      power_rate: 'arhehb',
    }
  }
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}
  let(:miner_slug) {miner_speed.miner.slug}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {miner_slug: miner_slug}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {
          miner_slug: miner_speed.miner.slug,
          id: miner_speed.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe "GET #new" do
        it "returns a success response" do
          get :new, params: {miner_slug: miner_slug}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "GET #edit" do
        it "returns a success response" do
          get :edit, params: {
            miner_slug: miner_slug,
            id: miner_speed.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new MinerSpeed" do
            expect {
              post :create, params: {miner_slug: miner_slug, MinerSpeed: valid_attributes}, session: valid_session
            }.to change(MinerSpeed, :count).by(0)
          end

          it "redirects to the created MinerSpeed" do
            post :create, params: {miner_slug: miner_slug, MinerSpeed: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{hash_rate: 666}}

          it "updates the requested MinerSpeed" do
            old_hash_rate = miner_speed.hash_rate
            put :update, params: {
              miner_slug: miner_slug,
              id: miner_speed.to_param,
              MinerSpeed: new_attributes
            }, session: valid_session
            miner_speed.reload
            expect(miner_speed.hash_rate).to eq old_hash_rate
          end

          it "redirects to the MinerSpeed" do
            put :update, params: {
              miner_slug: miner_slug,
              id: miner_speed.to_param,
              MinerSpeed: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested MinerSpeed" do
          miner_speed
          expect {
            delete :destroy, params: {
              miner_slug: miner_slug,
              id: miner_speed.to_param
            }, session: valid_session
          }.to change(MinerSpeed, :count).by(0)
        end

        it "redirects to the MinerSpeed list" do
          delete :destroy, params: {
            miner_slug: miner_slug,
            id: miner_speed.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}

    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {miner_slug: miner_slug}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {miner_slug: miner_slug, id: miner_speed.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new MinerSpeed" do
          expect {
            post :create, params: {miner_slug: miner_slug, miner_speed: valid_attributes}, session: valid_session
          }.to change(MinerSpeed, :count).by(1)
        end

        it "redirects to the created miner_speed" do
          post :create, params: {miner_slug: miner_slug, miner_speed: valid_attributes}, session: valid_session
          expect(response).to redirect_to(miner_speed_path(MinerSpeed.last.miner, MinerSpeed.last))
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {miner_slug: miner_slug, miner_speed: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
            hash_rate: 777,
            hash_rate_dual: 7777,
            power_rate: 77777,
          }
        }

        it "updates the requested miner_speed" do
          put :update, params: {miner_slug: miner_slug, id: miner_speed.to_param, miner_speed: new_attributes}, session: valid_session
          miner_speed.reload
          expect(miner_speed.hash_rate).to eq(new_attributes[:hash_rate])
          expect(miner_speed.hash_rate_dual).to eq(new_attributes[:hash_rate_dual])
          expect(miner_speed.power_rate).to eq(new_attributes[:power_rate])
        end

        it "redirects to the miner_speed" do
          put :update, params: {
            miner_slug: miner_speed.miner.slug,
            id: miner_speed.to_param,
            miner_speed: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(miner_speed_path(miner_speed.miner, miner_speed))
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested miner_speed" do

        expect {
          delete :destroy, params: {miner_slug: miner_slug, id: miner_speed.to_param}, session: valid_session
        }.to change(MinerSpeed, :count).by(-1)
      end

      it "redirects to the miner_speeds list" do

        delete :destroy, params: {miner_slug: miner_slug, id: miner_speed.to_param}, session: valid_session
        expect(response).to redirect_to(miner_url(miner_speed.miner))
      end
    end
  end
end
