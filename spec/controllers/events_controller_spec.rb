require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  let(:money) { FactoryBot.create(:money) }
  let!(:event) {FactoryBot.create(:event, money: money)}
  let(:valid_attributes) {
    {
        name: 'BTC test',
        money: money,
        body:"MyText",
        website: "MyString",
        api_url: "MyString",
        api_type: "MyString",
    }
  }
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}
  let(:money_slug) { money.slug }

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: { money_slug: money_slug }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {
            money_slug: money_slug,
            event_slug: event.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: { money_slug: money_slug }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Event' do
            expect {
              post :create, params: { money_slug: money_slug, event: valid_attributes }, session: valid_session
            }.to change(Event, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {
                money_slug: money_slug,
                event: {name: 'name', code: 'TESYI'}
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Event" do
            expect {
              post :create, params: {
                  money_slug: money_slug,
                  event: valid_attributes
              }, session: valid_session
            }.to change(Event, :count).by(0)
          end

          it "redirects to the created Event" do
            post :create, params: {
                money_slug: money_slug,
                event: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested Event" do
            old_name = event.name
            put :update, params: {
                money_slug: money_slug,
                event_slug: event.to_param,
                event: new_attributes
            }, session: valid_session
            event.reload
            expect(event.name).to eq old_name
          end

          it "redirects to the Event" do
            put :update, params: {
                money_slug: money_slug,
                event_slug:event.to_param,
                event: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Event" do
          expect {
            delete :destroy, params: {
                money_slug: money_slug,
                event_slug:event.to_param
            }, session: valid_session
          }.to change(Event, :count).by(0)
        end

        it "redirects to the Event list" do
          delete :destroy, params: {
              money_slug: money_slug,
              event_slug:event.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {money_slug: money_slug}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {
            money_slug: money_slug,
            event_slug:event.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Event" do
          expect {
            post :create, params: {
                money_slug: money_slug,
                event: valid_attributes
            }, session: valid_session
          }.to change(Event, :count).by(1)
        end

        it "redirects to the created event" do
          post :create, params: {
              money_slug: money_slug,
              event: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(money_event_path(money, Event.last))
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
              name: 'Test test'
          }
        }

        it "updates the requested event" do
          put :update, params: {
              money_slug: money_slug,
              event_slug:event.to_param,
              event: new_attributes
          }, session: valid_session
          event.reload
          expect(event.name).to eq(new_attributes[:name])
        end

        it "redirects to the event" do
          put :update, params: {
              money_slug: money_slug,
              event_slug:event.to_param,
              event: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(money_event_path(money, event))
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested event" do
        expect {
          delete :destroy, params: {
              money_slug: money_slug,
              event_slug:event.to_param
          }, session: valid_session
        }.to change(Event, :count).by(-1)
      end

      it "redirects to the event list" do
        delete :destroy, params: {
            money_slug: money_slug,
            event_slug:event.to_param
        }, session: valid_session
        expect(response).to redirect_to(money_events_url(money))
      end
    end
  end
end