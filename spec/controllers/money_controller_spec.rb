require 'rails_helper'

RSpec.describe MoneyController, type: :controller do

  let(:money) { FactoryBot.create(:money) }
  let(:valid_attributes) { { name: 'BTC test', symbol: 'BTCTEST' } }
  let(:invalid_attributes) { { name: 'name', symbol: nil } }
  let(:user) { FactoryBot.create(:user) }
  let(:valid_session) { {} }

  context 'Anonymous user' do
    describe 'GET #index' do
      it 'returns a success response' do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe 'GET #show' do
      it 'returns a success response' do
        money
        get :show, params: {slug: money.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Money' do
            expect {
              post :create, params: {money: {
                name: 'name',
                symbol: "TE3SYI#{rand(10000)}",
                website: 'http://ya.ru',
                currency: 'dhgas',
                is_crypto: 1
              }}, session: valid_session
            }.to change(Money, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {money: {name: 'name', code: 'TESYI'} }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Money" do
            expect {
              post :create, params: {Money: valid_attributes}, session: valid_session
            }.to change(Money, :count).by(0)
          end

          it "redirects to the created Money" do
            post :create, params: {Money: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) { { name: 'ttiittllee' } }

          it "updates the requested Money" do
            old_name = money.name
            put :update, params: {slug: money.to_param, Money: new_attributes}, session: valid_session
            money.reload
            expect(money.name).to eq old_name
          end

          it "redirects to the Money" do
            put :update, params: {slug: money.to_param, Money: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Money" do
          money
          expect {
            delete :destroy, params: {slug: money.to_param}, session: valid_session
          }.to change(Money, :count).by(0)
        end

        it "redirects to the Money list" do
          delete :destroy, params: {slug: money.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end
  
  context 'Authorized User' do
    before(:each) { sign_in user }

    describe 'GET #new' do
      it 'returns a success response' do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe 'POST #create' do
      context 'with valid params' do
        it 'creates a new Money' do
          expect {
            post :create, params: {money: {
              name: 'name',
              symbol: "TE3SYI#{rand(10000)}",
              website: 'http://ya.ru',
              currency: 'dhgas',
              money_type_id: 1
            }}, session: valid_session
          }.to change(Money, :count).by(1)
        end

        it 'redirects to the created money' do
          post :create, params: {money: {name: 'name', symbol: 'TESYI'} }, session: valid_session
          expect(response).to redirect_to(Money.last)
        end
      end

      context 'with invalid params' do
        it 'returns a success response (i.e. to display the new template)' do
          post :create, params: {money: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe 'GET #edit' do
      it 'returns a success response' do
        get :edit, params: {slug: money.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe 'PUT #update' do
      context 'with valid params' do
        let(:new_attributes) {
          { name: 'New name', symbol: 'NEWCODE2' }
        }

        it 'updates the requested money' do
          put :update, params: {slug: money.to_param, money: new_attributes}, session: valid_session
          money.reload
          expect(money.name).to eq(new_attributes[:name])
          expect(money.symbol).to eq(new_attributes[:symbol])
        end

        it 'redirects to the money' do
          put :update, params: {slug: money.to_param, money: valid_attributes}, session: valid_session
          expect(response).to redirect_to(money)
        end
      end

      context 'with invalid params' do
        it 'returns a success response (i.e. to display the \'edit\' template)' do
          put :update, params: {slug: money.to_param, money: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe 'DELETE #destroy' do
      it 'destroys the requested money' do
        money
        expect {
          delete :destroy, params: {slug: money.to_param}, session: valid_session
        }.to change(Money, :count).by(-1)
      end

      it 'redirects to the money list' do
        delete :destroy, params: {slug: money.to_param}, session: valid_session
        expect(response).to redirect_to(money_index_url)
      end
    end
  end
end
