require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do

  let(:article) { FactoryBot.create(:article) }
  let(:article_category) { FactoryBot.create(:article_category) }
  let(:user) { FactoryBot.create(:user) }

  let(:valid_attributes) {
    {
      title: 'Article test',
      article_category_id: article_category.id,
      user_id: user.id
    }
  }
  let(:invalid_attributes) { { title: nil } }
  let(:valid_session) { {} }

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: article.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Article' do
            expect {
              post :create, params: valid_attributes, session: valid_session
            }.to change(Article, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {article: {title: 'title', code: 'TESYI'} }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Article" do
            expect {
              post :create, params: {Article: valid_attributes}, session: valid_session
            }.to change(Article, :count).by(0)
          end

          it "redirects to the created Article" do
            post :create, params: {Article: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) { { title: 'ttiittllee' } }

          it "updates the requested Article" do
            old_title = article.title
            put :update, params: {slug: article.to_param, Article: new_attributes}, session: valid_session
            article.reload
            expect(article.title).to eq old_title
          end

          it "redirects to the Article" do
            put :update, params: {slug: article.to_param, Article: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Article" do
          article
          expect {
            delete :destroy, params: {slug: article.to_param}, session: valid_session
          }.to change(Article, :count).by(0)
        end

        it "redirects to the Article list" do
          delete :destroy, params: {slug: article.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) { sign_in user }

    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {slug: article.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Article" do
          expect {
            post :create, params: {article: valid_attributes}, session: valid_session
          }.to change(Article, :count).by(1)
        end

        it "redirects to the created article" do
          post :create, params: {article: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Article.last)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {article: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {{ title: 'New title'}}

        it "updates the requested article" do
          put :update, params: {
            slug: article.to_param,
            article: new_attributes
          }, session: valid_session
          article.reload
          expect(article.title).to eq(new_attributes[:title])
        end

        it "redirects to the article" do
          put :update, params: {
            slug: article.to_param,
            article: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(article)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'edit' template)" do
          put :update, params: {
            slug: article.to_param,
            article: invalid_attributes
          }, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested article" do
        article
        expect {
          delete :destroy, params: {slug: article.to_param}, session: valid_session
        }.to change(Article, :count).by(-1)
      end

      it "redirects to the article_categories list" do
        delete :destroy, params: {slug: article.to_param}, session: valid_session
        expect(response).to redirect_to(articles_url)
      end
    end
  end
end
