require 'rails_helper'

RSpec.describe PagesController, type: :controller do
  let!(:page) { FactoryBot.create(:page) }
  let(:user) { FactoryBot.create(:user) }
  let(:valid_attributes) { { title: "Title title test #{rand(10000)}"} }

  let(:valid_session) { {} }

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {id: page.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Page' do
            expect {
              post :create, params: {page: valid_attributes}, session: valid_session
            }.to change(Page, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {page: {name: 'name', code: 'TESYI'}}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Page" do
            expect {
              post :create, params: {Page: valid_attributes}, session: valid_session
            }.to change(Page, :count).by(0)
          end

          it "redirects to the created Page" do
            post :create, params: {Page: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested Page" do
            old_name = page.name
            put :update, params: {
              id: page.to_param,
              Page: new_attributes
            }, session: valid_session
            page.reload
            expect(page.name).to eq old_name
          end

          it "redirects to the Page" do
            put :update, params: {
              id: page.to_param, Page: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Page" do
          page
          expect {
            delete :destroy, params: {id: page.to_param}, session: valid_session
          }.to change(Page, :count).by(0)
        end

        it "redirects to the Page list" do
          delete :destroy, params: {id: page.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}

    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {id: page.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Page" do
          expect {
            post :create, params: {page: valid_attributes}, session: valid_session
          }.to change(Page, :count).by(1)
        end

        it "redirects to the created page" do
          post :create, params: {page: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Page.last)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
            title: 'testtt good'
          }
        }

        it "updates the requested page" do
          put :update, params: {id: page.to_param, page: new_attributes}, session: valid_session
          page.reload
          expect(page.title).to eq(new_attributes[:title])
        end

        it "redirects to the page" do
          put :update, params: {id: page.to_param, page: valid_attributes}, session: valid_session
          expect(response).to redirect_to(page)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested page" do
        expect {
          delete :destroy, params: {id: page.to_param}, session: valid_session
        }.to change(Page, :count).by(-1)
      end

      it "redirects to the pages list" do
        delete :destroy, params: {id: page.to_param}, session: valid_session
        expect(response).to redirect_to(pages_url)
      end
    end
  end
end
