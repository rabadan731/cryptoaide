require 'rails_helper'

RSpec.describe WalletsController, type: :controller do

  let!(:wallet) {FactoryBot.create(:wallet)}
  let(:valid_attributes) {{name: 'BTC test'}}
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: wallet.to_param}, session: valid_session
        expect(response).to be_success
      end
    end
    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Wallet' do
            expect {
              post :create, params: {wallet: valid_attributes}, session: valid_session
            }.to change(Wallet, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {wallet: {name: 'name', code: 'TESYI'}}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Wallet" do
            expect {
              post :create, params: {Wallet: valid_attributes}, session: valid_session
            }.to change(Wallet, :count).by(0)
          end

          it "redirects to the created Wallet" do
            post :create, params: {Wallet: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested Wallet" do
            old_name = wallet.name
            put :update, params: {slug: wallet.to_param, Wallet: new_attributes}, session: valid_session
            wallet.reload
            expect(wallet.name).to eq old_name
          end

          it "redirects to the Wallet" do
            put :update, params: {slug: wallet.to_param, Wallet: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Wallet" do
          expect {
            delete :destroy, params: {slug: wallet.to_param}, session: valid_session
          }.to change(Wallet, :count).by(0)
        end

        it "redirects to the Wallet list" do
          delete :destroy, params: {slug: wallet.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {slug: wallet.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Wallet" do
          expect {
            post :create, params: {wallet: valid_attributes}, session: valid_session
          }.to change(Wallet, :count).by(1)
        end

        it "redirects to the created wallet" do
          post :create, params: {wallet: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Wallet.last)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
              name: 'Test test'
          }
        }

        it "updates the requested wallet" do
          put :update, params: {slug: wallet.to_param, wallet: new_attributes}, session: valid_session
          wallet.reload
          expect(wallet.name).to eq(new_attributes[:name])
        end

        it "redirects to the wallet" do
          put :update, params: {slug: wallet.to_param, wallet: valid_attributes}, session: valid_session
          expect(response).to redirect_to(wallet)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested wallet" do
        expect {
          delete :destroy, params: {slug: wallet.to_param}, session: valid_session
        }.to change(Wallet, :count).by(-1)
      end

      it "redirects to the pools list" do
        delete :destroy, params: {slug: wallet.to_param}, session: valid_session
        expect(response).to redirect_to(wallets_url)
      end
    end
  end
end