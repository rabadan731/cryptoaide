require 'rails_helper'

RSpec.describe NewsSourcesController, type: :controller do

  let!(:news_source) {FactoryBot.create(:news_source)}
  let(:valid_attributes) {{name: 'BTC test'}}
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}

  context 'Anonymous user' do

    context 'No access' do
      describe "GET #index" do
        it "returns a success response" do
          get :index, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "GET #show" do
        it "returns a success response" do
          get :show, params: {id: news_source.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new NewsSource' do
            expect {
              post :create, params: {news_source: valid_attributes}, session: valid_session
            }.to change(NewsSource, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {news_source: {name: 'name', code: 'TESYI'}}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new NewsSource" do
            expect {
              post :create, params: {NewsSource: valid_attributes}, session: valid_session
            }.to change(NewsSource, :count).by(0)
          end

          it "redirects to the created NewsSource" do
            post :create, params: {NewsSource: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'bla bla'}}

          it "updates the requested NewsSource" do
            old_name = news_source.name
            put :update, params: {id: news_source.to_param, NewsSource: new_attributes}, session: valid_session
            news_source.reload
            expect(news_source.name).to eq old_name
          end

          it "redirects to the NewsSource" do
            put :update, params: {id: news_source.to_param, NewsSource: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested NewsSource" do
          expect {
            delete :destroy, params: {id: news_source.to_param}, session: valid_session
          }.to change(NewsSource, :count).by(0)
        end

        it "redirects to the NewsSource list" do
          delete :destroy, params: {id: news_source.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {id: news_source.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new NewsSource" do
          expect {
            post :create, params: {news_source: valid_attributes}, session: valid_session
          }.to change(NewsSource, :count).by(1)
        end

        it "redirects to the created news_source" do
          post :create, params: {news_source: valid_attributes}, session: valid_session
          expect(response).to redirect_to(NewsSource.last)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {{name: 'bla vla'}}

        it "updates the requested news_source" do
          put :update, params: {id: news_source.to_param, news_source: new_attributes}, session: valid_session
          news_source.reload
          expect(news_source.name).to eq(new_attributes[:name])
        end

        it "redirects to the news_source" do
          put :update, params: {id: news_source.to_param, news_source: valid_attributes}, session: valid_session
          expect(response).to redirect_to(news_source)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested news_source" do
        expect {
          delete :destroy, params: {id: news_source.to_param}, session: valid_session
        }.to change(NewsSource, :count).by(-1)
      end

      it "redirects to the pools list" do
        delete :destroy, params: {id: news_source.to_param}, session: valid_session
        expect(response).to redirect_to(news_sources_url)
      end
    end
  end
end