require 'rails_helper'

RSpec.describe PageController, type: :controller do
  context 'Anonymous User' do
    describe 'GET #index' do
      it 'returns a success response' do
        get :index, params: {}
        expect(response).to be_success
      end
    end
  end
end
