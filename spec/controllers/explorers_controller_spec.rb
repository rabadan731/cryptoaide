require 'rails_helper'

RSpec.describe ExplorersController, type: :controller do
  let(:money) { FactoryBot.create(:money) }
  let!(:explorer) {FactoryBot.create(:explorer, money: money)}
  let(:valid_attributes) {
    {
        name: 'BTC test',
        money: money,
        body:"MyText",
        website: "MyString",
        api_url: "MyString",
        api_type: "MyString",
    }
  }
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}
  let(:money_slug) { money.slug }

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: { money_slug: money_slug }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {
          money_slug: money_slug,
          explorer_slug:explorer.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: { money_slug: money_slug }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Explorer' do
            expect {
              post :create, params: { money_slug: money_slug, explorer: valid_attributes }, session: valid_session
            }.to change(Explorer, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {
              money_slug: money_slug,
              explorer: {name: 'name', code: 'TESYI'}
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Explorer" do
            expect {
              post :create, params: {
                money_slug: money_slug,
                Explorer: valid_attributes
              }, session: valid_session
            }.to change(Explorer, :count).by(0)
          end

          it "redirects to the created Explorer" do
            post :create, params: {
              money_slug: money_slug,
              Explorer: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested Explorer" do
            old_name = explorer.name
            put :update, params: {
              money_slug: money_slug,
              explorer_slug:explorer.to_param,
              Explorer: new_attributes
            }, session: valid_session
            explorer.reload
            expect(explorer.name).to eq old_name
          end

          it "redirects to the Explorer" do
            put :update, params: {
              money_slug: money_slug,
              explorer_slug:explorer.to_param,
              Explorer: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Explorer" do
          expect {
            delete :destroy, params: {
              money_slug: money_slug,
              explorer_slug:explorer.to_param
            }, session: valid_session
          }.to change(Explorer, :count).by(0)
        end

        it "redirects to the Explorer list" do
          delete :destroy, params: {
            money_slug: money_slug,
            explorer_slug:explorer.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {money_slug: money_slug}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {
          money_slug: money_slug,
          explorer_slug:explorer.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Explorer" do
          expect {
            post :create, params: {
              money_slug: money_slug,
              explorer: valid_attributes
            }, session: valid_session
          }.to change(Explorer, :count).by(1)
        end

        it "redirects to the created explorer" do
          post :create, params: {
            money_slug: money_slug,
            explorer: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(money_explorer_path(money, Explorer.last))
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
              name: 'Test test'
          }
        }

        it "updates the requested explorer" do
          put :update, params: {
            money_slug: money_slug,
            explorer_slug:explorer.to_param,
            explorer: new_attributes
          }, session: valid_session
          explorer.reload
          expect(explorer.name).to eq(new_attributes[:name])
        end

        it "redirects to the explorer" do
          put :update, params: {
            money_slug: money_slug,
            explorer_slug:explorer.to_param,
            explorer: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(money_explorer_path(money, explorer))
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested explorer" do
        expect {
          delete :destroy, params: {
              money_slug: money_slug,
              explorer_slug:explorer.to_param
          }, session: valid_session
        }.to change(Explorer, :count).by(-1)
      end

      it "redirects to the explorer list" do
        delete :destroy, params: {
          money_slug: money_slug,
          explorer_slug:explorer.to_param
        }, session: valid_session
        expect(response).to redirect_to(money_explorers_url(money))
      end
    end
  end
end