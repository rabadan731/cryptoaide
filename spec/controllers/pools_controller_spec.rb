require 'rails_helper'

RSpec.describe PoolsController, type: :controller do

  let!(:pool) {FactoryBot.create(:pool)}
  let(:valid_attributes) {{name: 'BTC test'}}
  let(:invalid_attributes) {{a: 3}}
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: pool.to_param}, session: valid_session
        expect(response).to be_success
      end
    end
    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Pool' do
            expect {
              post :create, params: {pool: valid_attributes}, session: valid_session
            }.to change(Pool, :count).by(0)
          end

          it 'redirects to index' do
            post :create, params: {pool: {name: 'name', code: 'TESYI'}}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Pool" do
            expect {
              post :create, params: {Pool: valid_attributes}, session: valid_session
            }.to change(Pool, :count).by(0)
          end

          it "redirects to the created Pool" do
            post :create, params: {Pool: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested Pool" do
            old_name = pool.name
            put :update, params: {slug: pool.to_param, Pool: new_attributes}, session: valid_session
            pool.reload
            expect(pool.name).to eq old_name
          end

          it "redirects to the Pool" do
            put :update, params: {slug: pool.to_param, Pool: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Pool" do
          pool
          expect {
            delete :destroy, params: {slug: pool.to_param}, session: valid_session
          }.to change(Pool, :count).by(0)
        end

        it "redirects to the Pool list" do
          delete :destroy, params: {slug: pool.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {slug: pool.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Pool" do
          expect {
            post :create, params: {pool: valid_attributes}, session: valid_session
          }.to change(Pool, :count).by(1)
        end

        it "redirects to the created pool" do
          post :create, params: {pool: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Pool.last)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
            name: 'Test test'
          }
        }

        it "updates the requested pool" do
          put :update, params: {slug: pool.to_param, pool: new_attributes}, session: valid_session
          pool.reload
          expect(pool.name).to eq(new_attributes[:name])
        end

        it "redirects to the pool" do
          put :update, params: {slug: pool.to_param, pool: valid_attributes}, session: valid_session
          expect(response).to redirect_to(pool)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested pool" do
        expect {
          delete :destroy, params: {slug: pool.to_param}, session: valid_session
        }.to change(Pool, :count).by(-1)
      end

      it "redirects to the pools list" do
        delete :destroy, params: {slug: pool.to_param}, session: valid_session
        expect(response).to redirect_to(pools_url)
      end
    end
  end
end