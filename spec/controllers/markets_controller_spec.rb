require 'rails_helper'

RSpec.describe MarketsController, type: :controller do

  let(:market) { FactoryBot.create(:market) }
  let(:user) { FactoryBot.create(:user) }
  let(:valid_attributes) { { title: "Title title test" } }
  let(:invalid_attributes) { { title: nil } }
  let(:valid_session) { {} }

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {slug: market.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe "GET #new" do
        it "returns a success response" do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "GET #edit" do
        it "returns a success response" do
          get :edit, params: {slug: market.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new Market" do
            expect {
              post :create, params: {Market: valid_attributes}, session: valid_session
            }.to change(Market, :count).by(0)
          end

          it "redirects to the created Market" do
            post :create, params: {Market: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) { { title: 'ttiittllee' } }

          it "updates the requested Market" do
            old_title = market.title
            put :update, params: {slug: market.to_param, Market: new_attributes}, session: valid_session
            market.reload
            expect(market.title).to eq old_title
          end

          it "redirects to the Market" do
            put :update, params: {slug: market.to_param, Market: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested Market" do
          market
          expect {
            delete :destroy, params: {slug: market.to_param}, session: valid_session
          }.to change(Market, :count).by(0)
        end

        it "redirects to the Market list" do
          delete :destroy, params: {slug: market.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) { sign_in user }
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        market = Market.create! valid_attributes
        get :edit, params: {slug: market.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Market" do
          expect {
            post :create, params: {market: valid_attributes}, session: valid_session
          }.to change(Market, :count).by(1)
        end

        it "redirects to the created market" do
          post :create, params: {market: valid_attributes}, session: valid_session
          expect(response).to redirect_to(Market.last)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {market: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          { title: 'New MArket test' }
        }

        it "updates the requested market" do
          put :update, params: {slug: market.to_param, market: new_attributes}, session: valid_session
          market.reload
          expect(market.title).to eq(new_attributes[:title])
        end

        it "redirects to the market" do
          put :update, params: {slug: market.to_param, market: valid_attributes}, session: valid_session
          expect(response).to redirect_to(market)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'edit' template)" do
          put :update, params: {slug: market.to_param, market: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested market" do
        market
        expect {
          delete :destroy, params: {slug: market.to_param}, session: valid_session
        }.to change(Market, :count).by(-1)
      end

      it "redirects to the markets list" do
        delete :destroy, params: {slug: market.to_param}, session: valid_session
        expect(response).to redirect_to(markets_url)
      end
    end
  end
end
