require 'rails_helper'

RSpec.describe TelegramChanelsController, type: :controller do

  let!(:telegram_chanel) {FactoryBot.create(:telegram_chanel)}
  let(:valid_attributes) {{name: 'BTC test'}}
  let(:invalid_attributes) {{a: 3}}
  let(:user) { FactoryBot.create(:user) }
  let!(:social) { FactoryBot.create(:social) }
  let(:valid_session) {{}}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index,
            params: {social_slug: social.slug },
            session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show,
            params: {social_slug: social.slug, slug: telegram_chanel.to_param},
            session: valid_session
        expect(response).to be_success
      end
    end
    context 'No access' do
      describe 'GET #new' do
        it 'redirect to index' do
          get :new,
              params: {social_slug: social.slug},
              session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new TelegramChanel' do
            expect {
              post :create,
                   params: {social_slug: social.slug, telegram_chanel: valid_attributes},
                   session: valid_session
            }.to change(TelegramChanel, :count).by(0)
          end

          it 'redirects to index' do
            post :create,
                 params: {social_slug: social.slug, telegram_chanel: {name: 'name', code: 'TESYI'}},
                 session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new TelegramChanel" do
            expect {
              post :create,
                   params: {social_slug: social.slug, TelegramChanel: valid_attributes},
                   session: valid_session
            }.to change(TelegramChanel, :count).by(0)
          end

          it "redirects to the created TelegramChanel" do
            post :create,
                 params: {social_slug: social.slug, TelegramChanel: valid_attributes},
                 session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'ttiittllee'}}

          it "updates the requested TelegramChanel" do
            old_name = telegram_chanel.name
            put :update,
                params: {
                    social_slug: social.slug,
                    slug: telegram_chanel.to_param,
                    TelegramChanel: new_attributes
                },
                session: valid_session
            telegram_chanel.reload
            expect(telegram_chanel.name).to eq old_name
          end

          it "redirects to the TelegramChanel" do
            put :update, params: {
                social_slug: social.slug,
                slug: telegram_chanel.to_param,
                TelegramChanel: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested TelegramChanel" do
          expect {
            delete :destroy,
                   params: {
                       social_slug: social.slug,
                       slug: telegram_chanel.to_param
                   }, session: valid_session
          }.to change(TelegramChanel, :count).by(0)
        end

        it "redirects to the TelegramChanel list" do
          delete :destroy,
                 params: {social_slug: social.slug, slug: telegram_chanel.to_param},
                 session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}
    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {social_slug: social.slug}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit,
            params: {social_slug: social.slug, slug: telegram_chanel.to_param},
            session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new TelegramChanel" do
          expect {
            post :create,
                 params: {social_slug: telegram_chanel.social.slug, telegram_chanel: valid_attributes},
                 session: valid_session
          }.to change(TelegramChanel, :count).by(1)
        end

        it "redirects to the created telegram_chanel" do
          post :create,
               params: {social_slug: telegram_chanel.social.slug, telegram_chanel: valid_attributes},
               session: valid_session
          last_telegram = TelegramChanel.last
          expect(response).to redirect_to(social_telegram_path(last_telegram.social, last_telegram))
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) { { name: 'Test test' } }

        it "updates the requested telegram_chanel" do
          put :update,
              params: {
                  social_slug: social.slug,
                  slug: telegram_chanel.to_param,
                  telegram_chanel: new_attributes
              },
              session: valid_session
          telegram_chanel.reload
          expect(telegram_chanel.name).to eq(new_attributes[:name])
        end

        it "redirects to the telegram_chanel" do
          put :update,
              params: {
                  social_slug: social.slug,
                  slug: telegram_chanel.to_param,
                  telegram_chanel: valid_attributes
              }, session: valid_session
          expect(response).to redirect_to(social_telegram_path(telegram_chanel.social, telegram_chanel))
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested telegram_chanel" do
        expect {
          delete :destroy, params: {
              social_slug: social.slug,
              slug: telegram_chanel.to_param
          }, session: valid_session
        }.to change(TelegramChanel, :count).by(-1)
      end

      it "redirects to the pools list" do
        delete :destroy, params: {
            social_slug: social.slug,
            slug: telegram_chanel.to_param
        }, session: valid_session
        expect(response).to redirect_to(social_telegram_index_url)
      end
    end
  end
end