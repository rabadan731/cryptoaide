require 'rails_helper'

RSpec.describe MinerModesController, type: :controller do

  let!(:miner_mode) {FactoryBot.create(:miner_mode)}
  let(:valid_attributes) {
    {
      name: 'BTC test',
      miner: FactoryBot.create(:miner)
    }
  }
  let(:invalid_attributes) {
    {
      name: nil
    }
  }
  let(:user) {FactoryBot.create(:user)}
  let(:valid_session) {{}}
  let(:miner_slug) {miner_mode.miner.slug}

  context 'Anonymous user' do
    describe "GET #index" do
      it "returns a success response" do
        get :index, params: {miner_slug: miner_slug}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      it "returns a success response" do
        get :show, params: {
            miner_slug: miner_mode.miner.slug,
            slug: miner_mode.to_param
        }, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe "GET #new" do
        it "returns a success response" do
          get :new, params: {miner_slug: miner_slug}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "GET #edit" do
        it "returns a success response" do
          get :edit, params: {
              miner_slug: miner_slug,
              slug: miner_mode.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new MinerMode" do
            expect {
              post :create, params: {miner_slug: miner_slug, MinerMode: valid_attributes}, session: valid_session
            }.to change(MinerMode, :count).by(0)
          end

          it "redirects to the created MinerMode" do
            post :create, params: {miner_slug: miner_slug, MinerMode: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {{name: 'sdfgsdg'}}

          it "updates the requested MinerMode" do
            old_name = miner_mode.name
            put :update, params: {
                miner_slug: miner_slug,
                slug: miner_mode.to_param,
                MinerMode: new_attributes
            }, session: valid_session
            miner_mode.reload
            expect(miner_mode.name).to eq old_name
          end

          it "redirects to the MinerMode" do
            put :update, params: {
                miner_slug: miner_slug,
                slug: miner_mode.to_param,
                MinerMode: valid_attributes
            }, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested MinerMode" do
          miner_mode
          expect {
            delete :destroy, params: {
                miner_slug: miner_slug,
                slug: miner_mode.to_param
            }, session: valid_session
          }.to change(MinerMode, :count).by(0)
        end

        it "redirects to the MinerMode list" do
          delete :destroy, params: {
              miner_slug: miner_slug,
              slug: miner_mode.to_param
          }, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) {sign_in user}

    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {miner_slug: miner_slug}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        get :edit, params: {miner_slug: miner_slug, slug: miner_mode.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new MinerMode" do
          expect {
            post :create, params: {miner_slug: miner_slug, miner_mode: valid_attributes}, session: valid_session
          }.to change(MinerMode, :count).by(1)
        end

        it "redirects to the created miner_mode" do
          post :create, params: {miner_slug: miner_slug, miner_mode: valid_attributes}, session: valid_session
          expect(response).to redirect_to(miner_mode_path(MinerMode.last.miner, MinerMode.last))
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {miner_slug: miner_slug, miner_mode: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) {
          {
              name: 'new name'
          }
        }

        it "updates the requested miner_mode" do
          put :update,
              params: {miner_slug: miner_slug, slug: miner_mode.to_param, miner_mode: new_attributes},
              session: valid_session
          miner_mode.reload
          expect(miner_mode.name).to eq(new_attributes[:name])
        end

        it "redirects to the miner_mode" do
          put :update, params: {
              miner_slug: miner_mode.miner.slug,
              slug: miner_mode.to_param,
              miner_mode: valid_attributes
          }, session: valid_session
          expect(response).to redirect_to(miner_mode_path(miner_mode.miner, miner_mode))
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested miner_mode" do

        expect {
          delete :destroy, params: {miner_slug: miner_slug, slug: miner_mode.to_param}, session: valid_session
        }.to change(MinerMode, :count).by(-1)
      end

      it "redirects to the miner_speeds list" do

        delete :destroy, params: {miner_slug: miner_slug, slug: miner_mode.to_param}, session: valid_session
        expect(response).to redirect_to(miner_url(miner_mode.miner))
      end
    end
  end
end
