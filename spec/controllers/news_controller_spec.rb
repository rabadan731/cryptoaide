require 'rails_helper'

RSpec.describe NewsController, type: :controller do

  let(:news) { FactoryBot.create(:news) }
  let(:user) { FactoryBot.create(:user) }
  let(:valid_attributes) { { title: "Title title test #{rand(10000)}", user: user } }
  let(:invalid_attributes) { { title: nil } }
  let(:valid_session) { {} }

  context 'Anonymous User' do
    describe 'GET #index' do
      it 'returns a success response' do
        get :index, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe 'GET #show' do
      it 'returns a success response' do
        get :show, params: {slug: news.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    context 'No access' do
      describe "GET #new" do
        it "returns a success response" do
          get :new, params: {}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "GET #edit" do
        it "returns a success response" do
          get :edit, params: {slug: news.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new News" do
            expect {
              post :create, params: {news: valid_attributes}, session: valid_session
            }.to change(News, :count).by(0)
          end

          it "redirects to the created news" do
            post :create, params: {news: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) { { title: 'ttiittllee' } }

          it "updates the requested news" do
            old_title = news.title
            put :update, params: {slug: news.to_param, news: new_attributes}, session: valid_session
            news.reload
            expect(news.title).to eq old_title
          end

          it "redirects to the news" do
            news = News.create! valid_attributes
            put :update, params: {slug: news.to_param, news: valid_attributes}, session: valid_session
            expect(response).to redirect_to(root_path)
          end
        end
      end

      describe "DELETE #destroy" do
        it "destroys the requested news" do
          news = News.create! valid_attributes
          expect {
            delete :destroy, params: {slug: news.to_param}, session: valid_session
          }.to change(News, :count).by(0)
        end

        it "redirects to the news list" do
          news = News.create! valid_attributes
          delete :destroy, params: {slug: news.to_param}, session: valid_session
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context 'Authorized User' do
    before(:each) { sign_in user }


    describe "GET #new" do
      it "returns a success response" do
        get :new, params: {}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      it "returns a success response" do
        news = News.create! valid_attributes
        get :edit, params: {slug: news.to_param}, session: valid_session
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new News" do
          expect {
            post :create, params: {news: valid_attributes}, session: valid_session
          }.to change(News, :count).by(1)
        end

        it "redirects to the created news" do
          post :create, params: {news: valid_attributes}, session: valid_session
          expect(response).to redirect_to(News.last)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'new' template)" do
          post :create, params: {news: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        let(:new_attributes) { { title: 'New update attr' } }

        it "updates the requested news" do
          news = News.create! valid_attributes
          put :update, params: {slug: news.to_param, news: new_attributes}, session: valid_session
          news.reload
          expect(news.title).to eq new_attributes[:title]
        end

        it "redirects to the news" do
          news = News.create! valid_attributes
          put :update, params: {slug: news.to_param, news: valid_attributes}, session: valid_session
          expect(response).to redirect_to(news)
        end
      end

      context "with invalid params" do
        it "returns a success response (i.e. to display the 'edit' template)" do
          news = News.create! valid_attributes
          put :update, params: {slug: news.to_param, news: invalid_attributes}, session: valid_session
          expect(response).to be_success
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested news" do
        news = News.create! valid_attributes
        expect {
          delete :destroy, params: {slug: news.to_param}, session: valid_session
        }.to change(News, :count).by(-1)
      end

      it "redirects to the news list" do
        news = News.create! valid_attributes
        delete :destroy, params: {slug: news.to_param}, session: valid_session
        expect(response).to redirect_to(news_index_url)
      end
    end

  end
end
