FactoryBot.define do
  factory :miner_mode do
    sequence(:name) { |n| "miner ##{n}" }
    image "MyString"
    body "MyText"
    association :miner
    website "MyString"
    partner_link "MyString"
    price 1.5
  end
end
