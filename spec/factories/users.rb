FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "email_#{n}@example.com" }
    role 'admin'
    after(:build) { |u| u.password_confirmation = u.password = "123456"}
  end
end
