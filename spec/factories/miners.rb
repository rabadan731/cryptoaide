FactoryBot.define do
  factory :miner do
    sequence(:name) { |n| "miner ##{n}" }
    body "MyText"
    image "MyString"
    power_rate 100
    noise_level 1000
  end
end
