FactoryBot.define do
  factory :pool_money do
    association :pool
    association :money
    website "MyString"
    partner_link "MyString"
  end
end
