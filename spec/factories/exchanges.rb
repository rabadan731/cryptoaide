FactoryBot.define do
  factory :exchange do
    sequence(:title) { |n| "Обменик ##{n}" }
    body 'MyText MyText MyText MyText '
    website 'http://testik.ru'
    status 1
    course_link 'http://rabadan.ru'
    parser_type 'default'
    birth_date '2017-11-23 04:28:01'
  end
end
