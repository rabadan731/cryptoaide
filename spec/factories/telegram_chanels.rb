FactoryBot.define do
  factory :telegram_chanel do
    sequence(:name) { |n| "telegram_chanel ##{n}" }
    body "MyText"
    website "MyString"
    count_user 1
    type_chanel 1
    partner_link "MyString"
    association :social
  end
end
