FactoryBot.define do
  factory :article do
    sequence(:title) { |n| "Article ##{n}" }
    association :article_category
    body "MyText"
    association :user
    meta_key "MyString"
    meta_desc "MyString"
  end
end
