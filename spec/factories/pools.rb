FactoryBot.define do
  factory :pool do
    sequence(:name) { |n| "Pool ##{n}" }
    body "MyText"
    website "MyString"
  end
end
