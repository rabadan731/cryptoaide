FactoryBot.define do
  factory :event do
    association :money
    name "MyString"
    body "MyText"
    appointment_date "2018-01-06 13:43:54"
    description "MyText"
    status_text "MyString"
    progress 17
  end
end
