FactoryBot.define do
  factory :market do |i|
    sequence(:title) { |n| "Биржа ##{n}" }
    website 'http://btc.com'
    language 'ru'
    body 'MyText MyText MyText MyText MyText'
    twitter 'rabadan731'
  end
end