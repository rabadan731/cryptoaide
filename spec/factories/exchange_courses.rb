FactoryBot.define do
  factory :exchange_course do
    association :exchange
    association :money_from
    association :money_to
    amount_min "3"
    amount_max "777"
    amount_exist "5"
    value_in "10"
    value_out "20"
  end
end
