FactoryBot.define do
  factory :wallet do
    name "MyString"
    body "MyText"
    website "http://ya.ru"
    partner_link "http://rabadan.ru"
  end
end
