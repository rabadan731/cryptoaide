FactoryBot.define do
  factory :page do
    name "MyText"
    title "MyString"
    meta_description "MyText"
    meta_keywords "MyText"
    body "MyText"
    h1 "MyString"
    controller "MyString"
    action "MyString"
    simple_text "MyText"
  end
end
