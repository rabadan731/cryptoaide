FactoryBot.define do
  factory :news_source do
    name "MyString"
    source "MyString"
    website "MyString"
  end
end
