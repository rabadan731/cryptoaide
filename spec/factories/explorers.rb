FactoryBot.define do
  factory :explorer do
    name "MyString"
    body "MyText"
    association :money
    website "MyString"
    api_url "MyString"
    api_type "MyString"
  end
end
