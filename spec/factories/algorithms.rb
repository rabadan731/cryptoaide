FactoryBot.define do
  factory :algorithm do
    sequence(:name) { |n| "algorithm ##{n}" }
    body "MyText"
  end
end
