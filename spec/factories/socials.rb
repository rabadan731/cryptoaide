FactoryBot.define do
  factory :social do
    sequence(:name) { |n| "social ##{n}" }
    image "MyString"
    body "MyText"
  end
end
