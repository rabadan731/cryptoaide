FactoryBot.define do
  factory :wallet_money do
    association :wallet
    association :money
  end
end
