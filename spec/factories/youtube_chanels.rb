FactoryBot.define do
  factory :youtube_chanel do
    sequence(:name) { |n| "youtube_chanel ##{n}" }
    body "MyText"
    website "MyString"
    count_user 1
    partner_link "MyString"
    association :social
  end
end
