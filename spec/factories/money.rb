FactoryBot.define do
  factory :money do
    name "BITCOIN 23 fff"
    body "BITCOIN BITCOIN BITCOIN BITCOIN BITCOIN BITCOIN BITCOIN BITCOIN "
    website "http://rabadan.ru"
    sequence(:symbol) { |n| "BTC#{n}CODE" }
    twitter "bitcoin"
    money_type_id 1
    currency "BTC"
  end
end
