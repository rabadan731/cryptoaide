FactoryBot.define do
  factory :miner_speed do
    association :miner
    association :algorithm
    algorithm_dual { FactoryBot.create(:algorithm) }
    hash_rate 1000
    hash_rate_dual 100
    power_rate 10
  end
end
