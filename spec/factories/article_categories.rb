FactoryBot.define do
  factory :article_category do
    sequence(:title) { |n| "Category news title ##{n}" }
    body "Category news title Category news title "
    meta_desc "MyString"
    meta_key "MyString"
    image "MyString"
  end
end
