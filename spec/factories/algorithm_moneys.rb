FactoryBot.define do
  factory :algorithm_money do
    association :algorithm
    association :money
  end
end
