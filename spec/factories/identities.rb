FactoryBot.define do
  factory :identity do
    association :user
    provider "MyString"
    uid "MyString"
  end
end
