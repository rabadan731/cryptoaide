FactoryBot.define do
  factory :news do
    sequence(:title) { |n| "Новости #{n}" }
    meta_description 'MyText'
    meta_keywors 'MyText'
    body 'MyText'
    association :user
  end
end
