FactoryBot.define do
  factory :go_counter do
    resource_class "MyString"
    resource_id 1
    date_go "2018-01-06 19:55:42"
    user_agent "MyText"
    ip "MyString"
  end
end
