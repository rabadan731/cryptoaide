source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.6'

gem 'rails', '~> 5.1.4'
gem 'rails-i18n', '~> 5.0'

gem 'devise', '~> 4.3'
gem 'devise-i18n', '~> 1.2'

gem 'omniauth'
gem 'omniauth-vkontakte'
gem 'omniauth-facebook'
gem 'omniauth-twitter'

gem 'translit'

gem 'kaminari'
gem 'sitemap_generator'
gem 'breadcrumbs_on_rails'
gem 'acts-as-taggable-on'
gem 'audited', '~> 4.6'
gem 'commontator', '~> 5.1.0'

gem 'yt', '~> 0.28.0'
gem 'ckeditor', '~> 4.2.4'
gem 'carrierwave', '~> 1.0'
gem 'rmagick'
gem 'mini_magick'
gem 'simple_form'
gem 'activerecord-import'

gem 'sass-rails', '~> 5.0'
gem 'bootstrap-sass', '~> 3.3'
gem 'font-awesome-rails', '~> 4.7'
gem 'jquery-rails', '~> 4.3'
gem 'coffee-rails', '~> 4.2'
# gem 'therubyracer'
gem 'mini_racer'

gem 'pundit', '~> 1.1'
gem 'puma', '~> 3.7'
gem 'uglifier', '>= 1.3.0'
gem 'nokogiri'

gem 'redis-rails'
gem 'sidekiq', '~> 5.2.2'
gem 'sidekiq-scheduler', '~> 3.0.0'
gem 'rufus-scheduler', '~> 3.5.2'

gem 'telegram-bot'

group :development, :test do
  gem 'sqlite3'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  gem 'selenium-webdriver', '~> 3.6'
  gem 'rspec-rails', '~> 3.4'
  gem 'factory_bot_rails', '~> 4.8'
  gem 'shoulda-matchers'
end

group :development do
  gem 'capistrano', '~> 3.11.0'
  gem 'capistrano-bundler', '~> 1.3'
  gem 'capistrano-passenger', '~> 0.2'
  gem 'capistrano-rails', '~> 1.3'
  gem 'capistrano-rbenv', '~> 2.1'
  gem 'capistrano-sidekiq'

  gem 'letter_opener', '~> 1.4'
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring', '~> 2.0'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :production do
  gem 'pg'
end

group :test do
  gem 'capybara', '~> 2.15'
  gem 'launchy', '~> 2.4'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
