require 'parser/coinmarketcap'

class LoadMoneysJob < ApplicationJob
  queue_as :default

  def perform(count = 12)
    CoinMarketCap.all_currency(count)
  end
end
