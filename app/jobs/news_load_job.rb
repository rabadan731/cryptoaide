require 'parser/news_parser'
class NewsLoadJob < ApplicationJob
  queue_as :default

  def perform
    NewsParser.load_all_news
  end
end
