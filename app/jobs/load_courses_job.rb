require 'parser/course'

class LoadCoursesJob < ApplicationJob
  queue_as :default

  def perform
    Exchange.active.all.each do |exchange|
      Course.new(exchange).parse
      sleep 5
    end

    ExchangeCourse.where("exchange_courses.created_at < ?", 20.minutes.ago).delete_all
  end
end
