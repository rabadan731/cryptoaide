module MinersHelper
  def miner_type_list
    {
        'ASIC' => 2,
        'GPU' => 4,
        'CPU' => 6,
        'RISER' => 8,
        'MATHER' => 10,
        'POWER' => 12,
        'OTHERS' => 100,
    }
  end
end
