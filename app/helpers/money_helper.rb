module MoneyHelper
  def nice_price(price, precision = nil)
    return 0 unless price.present?

    if precision.nil?
      case
        when price >= 1000
          precision = 0
        when price >= 100
          precision = 2
        when price >= 10
          precision = 4
        when price >= 1
          precision = 6
        else
          precision = 8
      end
    end

    number_to_currency(
      price,
      unit: '',
      delimiter: ' ',
      precision: precision,
      separator: '.'
    )
  end

  def currency_list
    fiat_list + Money.select('symbol').all.map(&:symbol)
  end

  def fiat_list
    [
      :RUB,
      :USD,
      :EUR,
      :KZT,
      :UAH,
    ]
  end

  def money_type_list
    {
      'Криптовалюта' => 1,
      'Банк' => 2,
      'Биржа' => 3,
      'Элекртонные деньги' => 4,
      'Денежный перевод' => 5,
      'Наличные' => 6,
    }
  end

  def money_title_from_currency(currency)
    unless fiat_list.include?(currency)
      money = Money.find_by_symbol(currency)
      return money.name if money.present?
    end
    t("crypto.#{currency}")
  end

  def page_text(name)
    page = Page.find_or_create_by!(controller: 'money_mining', action: name)
  end
end
