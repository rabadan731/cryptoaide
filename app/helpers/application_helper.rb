module ApplicationHelper
  require 'guest_user'
  def bootstrap_class_for(flash_type)
    case flash_type.to_sym
      when :error
        'alert-danger'
      when :alert
        'alert-danger'
      when :notice
        'alert-info'
      else
        "alert-#{flash_type}"
    end
  end

  def is_admin?
    current_user.present? && current_user.admin?
  end

  def current_user_or_guest
    user_signed_in? ?
      link_to(current_user.name, user_show_path(id: current_user.id)) :
      link_to(t('activerecord.attributes.user.guest'), new_user_session_path)
  end

  def meta_og_tags(properties = {})
    return unless properties.is_a? Hash

    tags = []

    properties.each do |property, value|
      tags << tag(:meta, property: "og:#{property}", content: value) if value.present?
    end

    tags << tag(:meta, property: "og:locale", content: 'ru_RU')
    tags.join.html_safe  # Remember in Ruby the last line is returned
  end

  def favicon_from_action(action)
    if action == 'create'
      'fa fa-plus'
    elsif action == 'destroy'
      'fa fa-trash'
    elsif action == 'update'
      'fa fa-pencil'
    else
      'fa fa-info'
    end
  end

  def class_bg_from_action(action)
    if action == 'create'
      'bg-primary'
    elsif action == 'destroy'
      'bg-danger'
    elsif action == 'update'
      'bg-success'
    else
      'bg-warning'
    end
  end
end
