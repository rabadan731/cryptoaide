class Algorithm < ApplicationRecord
  has_many :algorithm_moneys, :dependent => :destroy
  has_many :moneys, through: :algorithm_moneys

  before_validation :set_slug
  mount_uploader :image, AlgorithmUploader
  acts_as_taggable
  audited

  validates :name, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank?
  end
end
