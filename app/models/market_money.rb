class MarketMoney < ApplicationRecord
  belongs_to :market, -> { order('name') }
  belongs_to :money, -> { order('name') }

  audited
end
