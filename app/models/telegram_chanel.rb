class TelegramChanel < ApplicationRecord
  belongs_to :social

  before_validation :set_slug

  validates :name, presence: true
  validates :slug, uniqueness: true

  TYPES_CHANEL = [:type_group, :type_public, :type_bot]

  enum type_chanel: TYPES_CHANEL

  acts_as_taggable
  acts_as_commontable dependent: :destroy

  audited

  def to_param
    slug
  end

  def go_commit(ip)
    GoCounter.create({
      resource_class: self.class,
      resource_id:self.id,
      date_go: Time.now,
      ip: ip
    })
  end

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank?
  end
end
