class Social < ApplicationRecord
  has_many :telegram_chanels
  has_many :youtube_chanels
  before_validation :set_slug

  validates :name, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  audited

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank? && self.name.present?
  end
end
