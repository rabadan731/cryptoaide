class News < ApplicationRecord
  scope :latest, -> (count = 5) { order(created_at: :desc).limit(count) }
  scope :published, -> { where(status: 7) }
  
  belongs_to :user
  before_validation :set_slug

  acts_as_taggable
  acts_as_commontable dependent: :destroy

  mount_uploader :image, NewsUploader

  validates :title, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  def all_comments
    parent_comments = Comment.where(:commentable_id => id)
    child_comments = Comment.where(:commentable_id => parent_comments.map(&:id))
    parent_comments + child_comments
  end

  private

  def set_slug
    self.slug = to_slug(self.title) if self.slug.blank?
  end
end
