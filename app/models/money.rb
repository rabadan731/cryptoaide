class Money < ApplicationRecord
  has_many :exchange_course_from, foreign_key: "money_from_id", class_name: "ExchangeCourse"
  has_many :exchange_course_to, foreign_key: "money_to_id", class_name: "ExchangeCourse"
  has_many :best_course, -> {
    select("exchange_courses.money_to_id", "MAX(exchange_courses.value_out) AS value_max")
    .group("money_to_id")
  }, foreign_key: "money_from_id", class_name: "ExchangeCourse"

  has_many :miner_speeds, :through => :algorithm_moneys, source: :algorithm
  # has_many :miners, through: :miner_speeds

  has_many :algorithm_moneys
  has_many :algorithms, -> { order('name') }, through: :algorithm_moneys

  has_many :pool_moneys
  has_many :pools, -> { order('name') }, through: :pool_moneys

  has_many :wallet_moneys
  has_many :wallets, -> { order('name') }, through: :wallet_moneys

  has_many :market_moneys
  has_many :markets, -> { order('name') }, through: :market_moneys

  has_many :events

  scope :is_crypto, -> { where(money_type_id: 1) }
  scope :is_not_crypto, -> { where.not(money_type_id: 1) }
  scope :currency_list, -> { select(:currency).distinct.is_not_crypto }
  scope :change_sell, -> { order(:money_type_id, :name).find(ExchangeCourse.money_from_ids.all.map(&:money_from_id)) }
  scope :change_buy, -> { order(:money_type_id, :name).find(ExchangeCourse.money_to_ids.all.map(&:money_to_id)) }

  mount_uploader :image, MoneyUploader
  acts_as_taggable
  audited only: [:name, :symbol, :body, :money_type_id]

  before_validation :set_slug
  validates :name, :symbol, presence: true
  validates :slug, uniqueness: true
  validates :symbol, uniqueness: true

  def to_param
    slug
  end

  def icon_name
    "<img class='img-circle' src='#{image.icon.url}' alt='' /> #{name}".html_safe
  end

  def is_crypto?
    money_type_id == 1
  end

  def change_1h_up?
    if percent_change_1h.present?
      return percent_change_1h > 0
    end
    false
  end

  def change_24h_up?
    if percent_change_24h.present?
      return percent_change_24h > 0
    end
    false
  end

  def change_7d_up?
    if percent_change_7d.present?
      return percent_change_7d > 0
    end
    false
  end

  def miners
    miner_ids = MinerSpeed.select('miner_id').where(algorithm_id: algorithm_ids).map(&:miner_id)
    Miner.where(id: miner_ids).all
  end

  def profitability_per_day
    require 'parser/whattomine_parse'

    speeds = MinerSpeed.where(algorithm_id: algorithm_ids)
    parser = WhattomineParse.new
    speeds.each do |item|
      parser.set_algorithm(item.slug, item.hash_rate, item.power_rate)
    end

    parser.get_average_profit
  end

  def is_mining?
    miner_count = MinerSpeed.where(algorithm_id: self.algorithm_moneys.select('algorithm_id')).count
    is_crypto? &&
      self.pool_moneys.count > 0 &&
      miner_count > 0
  end

  def event_exist?
    self.events.count > 0
  end

  def self.find_by_symbol_or_create(symbol)
    money_from = Money.find_by_symbol(symbol)
    if money_from.nil?
      money_from = Money.new

      if ['RUB', 'USD', 'EUR', 'KZT', 'UAH'].include?(symbol.last(3))
        money_from.currency = symbol.last(3)
      else
        money_from.currency = symbol
        money_from.money_type_id = 1
      end

      money_from.symbol = symbol

      money_from.name = "Money ##{symbol}"
      money_from.save
    end
    money_from
  end



  def algorithm_ids
    self.algorithm_moneys.select('algorithm_id')
  end

  private

  def set_slug
    self.slug = to_slug(self.symbol) if self.slug.blank? && self.symbol.present?
  end
end
