class Event < ApplicationRecord
  belongs_to :money

  before_validation :set_slug
  acts_as_taggable
  audited

  validates :name, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank?
  end
end
