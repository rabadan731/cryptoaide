class YoutubeChanel < ApplicationRecord
  belongs_to :social

  before_validation :set_slug

  validates :slug, uniqueness: true

  def to_param
    slug
  end

  def go_commit(ip)
    GoCounter.create({
      resource_class: self.class,
      resource_id: self.id,
      date_go: Time.now,
      ip: ip
    })
  end

  def yt
    Yt::Channel.new id: self.chanel_id
  end

  audited

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank?
  end
end
