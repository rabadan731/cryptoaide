class AlgorithmMoney < ApplicationRecord
  belongs_to :algorithm
  belongs_to :money

  audited
end
