class MinerMode < ApplicationRecord
  before_validation :set_slug
  mount_uploader :image, MinerModeUploader

  belongs_to :miner
  audited

  validates :name, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank?
  end
end
