class PoolMoney < ApplicationRecord
  MINING_TYPES = [:solo_mining, :multiple_mining]
  belongs_to :pool, -> { order('name') }
  belongs_to :money, -> { order('name') }

  audited

  enum mining_type: MINING_TYPES
end
