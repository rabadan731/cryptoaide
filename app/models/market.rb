class Market < ApplicationRecord
  before_validation :set_slug
  mount_uploader :image, MarketUploader

  acts_as_taggable
  audited

  validates :title, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  def go_commit(ip)
    GoCounter.create({
         resource_class: self.class,
         resource_id:self.id,
         date_go: Time.now,
         ip: ip
     })
  end

  private

  def set_slug
    self.slug = to_slug(self.title) if self.slug.blank?
  end
end
