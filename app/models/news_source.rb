class NewsSource < ApplicationRecord
  scope :active, -> { where(status: 1) }

  audited
end
