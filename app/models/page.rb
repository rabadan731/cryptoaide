class Page < ApplicationRecord
  audited


  def replace_data(object)
    [
      self.title,
      self.h1,
      self.meta_description,
      self.meta_keywords,
      self.body,
      self.simple_text
    ].each do |content|
      next if content.nil?
      content.scan(/\{.*?}/).map do |replace_tag|
        method_name = (replace_tag.gsub('{', '').gsub('}', '')).to_sym
        if object.present? && object.respond_to?(method_name)
          content.gsub!(replace_tag, "#{object.send(method_name)}")
        end
      end
    end

    self
  end
end
