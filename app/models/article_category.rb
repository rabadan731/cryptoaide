class ArticleCategory < ApplicationRecord
  before_validation :set_slug

  validates :title, presence: true
  validates :slug, uniqueness: true

  audited

  def to_param
    slug
  end

  private

  def set_slug
    self.slug = to_slug(self.title) if self.slug.blank?
  end
end
