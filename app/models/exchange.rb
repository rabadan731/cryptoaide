class Exchange < ApplicationRecord
  scope :active, -> { where(status: 1) }
  before_validation :set_slug
  mount_uploader :image, ExchangeUploader

  acts_as_taggable
  audited

  validates :title, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  def full_reserve
    reserve = 0

    exchange_courcses = ExchangeCourse.select('money_to_id,currency_to, amount_exist')
                            .where(exchange_id: id)
                            .where('amount_exist > 0')
                            .group('money_to_id,currency_to,amount_exist')

    exchange_courcses.each do |exchange_courcse|

      if exchange_courcse.currency_to == 'USD'
        reserve += exchange_courcse.amount_exist
      else
        course = ExchangeCourse
                     .where(currency_from: exchange_courcse.money_to.currency)
                     .where(currency_to: 'USD')
                     .select('AVG((value_in/value_out)) as value_out')
                     .find_by(exchange_id: id)
                     .value_out
        if course.nil?
          course = ExchangeCourse
             .where(currency_from: exchange_courcse.money_to.currency)
             .where(currency_to: 'USD')
             .select('AVG((value_in/value_out)) as value_out')
             .take
             .value_out
        end
        course = 0 if course.nil?

        reserve += exchange_courcse.amount_exist / course if course > 0
      end
    end

    reserve
  end

  def go_commit(ip)
    GoCounter.create({
     resource_class: self.class,
     resource_id:self.id,
     date_go: Time.now,
     ip: ip
    })
  end

  private

  def set_slug
    self.slug = to_slug(self.title) if self.slug.blank?
  end
end
