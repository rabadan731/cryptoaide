class Pool < ApplicationRecord
  has_many :pool_moneys, :dependent => :destroy
  has_many :moneys, -> { order('name') }, through: :pool_moneys
  before_validation :set_slug

  validates :name, presence: true
  validates :slug, uniqueness: true

  audited

  def to_param
    slug
  end

  def go_commit(ip)
    GoCounter.create({
       resource_class: self.class,
       resource_id:self.id,
       date_go: Time.now,
       ip: ip
   })
  end

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank? && self.name.present?
  end
end
