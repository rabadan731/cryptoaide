class Miner < ApplicationRecord
  has_many :miner_speeds, :dependent => :destroy
  has_many :miner_modes, :dependent => :destroy
  has_many :algorithms, through: :miner_speeds

  mount_uploader :image, MinerUploader
  audited

  before_validation :set_slug

  validates :name, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  def profitability
    w = WhattomineParse.new
    self.miner_speeds.all.each do |item|
      w.set_algorithm(item.algorithm.slug, item.hash_rate / 100, item.power_rate)
    end
    w.get_average_profit
  end

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank? && self.name.present?
  end
end
