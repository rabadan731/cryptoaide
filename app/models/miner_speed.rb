class MinerSpeed < ApplicationRecord
  belongs_to :miner
  belongs_to :algorithm, -> { order('name') }
  belongs_to :algorithm_dual, foreign_key: "algorithm_dual_id", class_name: "Algorithm", optional: true
  audited

  def name
    return "#{algorithm.name} / #{algorithm_dual.name}" if algorithm_dual.present?
    "#{algorithm.name}"
  end
end
