class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true


  def to_slug(text)
    # strip the string
    new_slug = text.strip
    new_slug = Translit.convert(new_slug, :english)
    #blow away apostrophes
    new_slug.gsub! /['`\.]/, ""

    # @ --> at, and & --> and
    new_slug.gsub! /\s*@\s*/, " at "
    new_slug.gsub! /\s*&\s*/, " and "

    # replace all non alphanumeric, periods with dash
    new_slug.gsub! /\s*[^A-Za-z0-9\.]\s*/, '-'

    # replace underscore with dash
    new_slug.gsub! /[-_]{2,}/, '-'

    # convert double dashes to single
    new_slug.gsub! /-+/, "-"

    # strip off leading/trailing dash
    new_slug.gsub! /\A[-\.]+|[-\.]+\z/, ""

    new_slug.downcase!

    new_slug
  end
end
