class Wallet < ApplicationRecord
  has_many :wallet_moneys, :dependent => :destroy
  has_many :moneys, through: :wallet_moneys
  before_validation :set_slug

  validates :name, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  def go_commit(ip)
    GoCounter.create({
      resource_class: self.class,
      resource_id:self.id,
      date_go: Time.now,
      ip: ip
    })
  end

  audited

  private

  def set_slug
    self.slug = to_slug(self.name) if self.slug.blank? && self.name.present?
  end
end
