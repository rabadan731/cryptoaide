class WalletMoney < ApplicationRecord
  belongs_to :wallet
  belongs_to :money

  audited
end
