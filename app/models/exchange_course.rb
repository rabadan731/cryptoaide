class ExchangeCourse < ApplicationRecord
  belongs_to :exchange
  belongs_to :money_from, class_name: 'Money'
  belongs_to :money_to, class_name: 'Money'

  scope :courses_buy, -> (money) { select('currency, min((value_in*1.1) / (value_out*1.1)) as val')
                                     .left_joins(:money_from)
                                     .where(money_to: money)
                                     .where.not(currency_from: money.currency)
                                     .order('currency')
                                     .group('currency') }

  scope :courses_sell, -> (money) { select('currency, max((value_out*1.1)/(value_in*1.1)) as val')
                                       .left_joins(:money_to)
                                       .where(money_from: money)
                                       .where.not(currency_to: money.currency)
                                       .order('currency')
                                       .group('currency') }

  scope :money_from_ids, -> { select(:money_from_id).distinct } #
  scope :money_to_ids, -> { select(:money_to_id).distinct } #
end
