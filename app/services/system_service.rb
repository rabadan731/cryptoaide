class SystemService
  def self.restart_workers
    # system "redis-server &" if !redis_connected?
    ps = Sidekiq::ProcessSet.new
    process_size = ps.size.to_i
    if process_size <= 0
      be = '$HOME/.rbenv/bin/rbenv exec bundle exec'

      system "rm #{Rails.root}/tmp/pids/*.pid &"

      i = 0
      command_run1 = "#{be} sidekiq --index #{i}"
      command_run1 += " --pidfile #{Rails.root}/tmp/pids/sidekiq-#{i}.pid"
      command_run1 += " --environment #{Rails.env}"
      command_run1 += " --logfile #{Rails.root}/log/sidekiq#{i}.log --daemon &"
      system command_run1

      i = 1
      command_run1 = "#{be} sidekiq --index #{i}"
      command_run1 += " --pidfile #{Rails.root}/tmp/pids/sidekiq-#{i}.pid"
      command_run1 += " --environment #{Rails.env}"
      command_run1 += " --logfile #{Rails.root}/log/sidekiq#{i}.log --daemon"
      system command_run1
    end
  end
end