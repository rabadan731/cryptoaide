class PoolMoneysController < ApplicationController
  before_action :set_pool_money, only: [:show, :edit, :update, :destroy]
  before_action :set_pool


  # GET /miner_speeds
  def index
    @pool_moneys = PoolMoney.all
  end

  # pool_pool_money_go_exchange
  def go
    @pool_moneys = PoolMoney.find(params[:money_id])
    if @pool_moneys.partner_link.present?
      redirect_to(@pool_moneys.partner_link)
    else
      if @pool_moneys.website.present?
        redirect_to(@pool_moneys.website)
      else
        redirect_to(@pool_moneys.pool.website)
      end
    end
  end

  # GET /pool_moneys/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.pool'), pools_path
    add_breadcrumb @pool.name, pool_path(@pool)
    set_meta(I18n.t('activerecord.models.pool_money', pool: @pool.name, money: @pool_money.money.name))
    add_breadcrumb @h1
  end

  # GET /pool_moneys/new
  def new
    authorize PoolMoney
    @pool_money = PoolMoney.new
    @pool_money.pool = @pool

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.pool_money'), pool_moneys_path
    add_breadcrumb @h1
  end

  # GET /pool_moneys/1/edit
  def edit
    authorize @pool_money
  end

  # POST /pool_moneys
  def create
    authorize PoolMoney
    @pool_money = PoolMoney.new(pool_money_params)
    @pool_money.pool = @pool
    if @pool_money.save
      redirect_to pool_money_path(@pool, @pool_money), notice: 'Pool money was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /pool_moneys/1
  def update
    authorize @pool_money
    if @pool_money.update(pool_money_params)
      redirect_to pool_money_path(@pool_money.pool, @pool_money), notice: 'Pool money was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /pool_moneys/1
  def destroy
    authorize @pool_money
    @pool_money.destroy
    redirect_to pool_path(@pool), notice: 'Pool money was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_pool_money
    @pool_money = PoolMoney.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def pool_money_params
    params.require(:pool_money).permit(
        :pool_id,
        :money_id,
        :body,
        :website,
        :partner_link,
        :pool_hashrate,
        :miners_count,
        :workers_count,
        :blocks_hour,
        :mining_type,
    )
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_pool
    @pool = Pool.find_by_slug(params[:pool_slug])
  end
end
