class TelegramChanelsController < ApplicationController
  before_action :set_telegram_chanel, only: [:show, :edit, :update, :destroy]
  before_action :set_social


  def go
    @telegram_chanel = TelegramChanel.find_by(slug: params[:telegram_chanel_slug])
    if @telegram_chanel.partner_link.present?
      @telegram_chanel.go_commit(request.remote_ip)
      redirect_to(@telegram_chanel.partner_link)
    else
      if @telegram_chanel.website.present?
        @telegram_chanel.go_commit(request.remote_ip)
        redirect_to(@telegram_chanel.website)
      else
        redirect_to telegram_chanel_path(@telegram_chanel)
      end
    end
  end

  # GET /telegram_chanels
  def index
    @telegram_chanels = TelegramChanel.page params[:page]

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    set_meta(I18n.t('activerecord.models.telegram_chanels'))
    add_breadcrumb @h1
  end

  # GET /telegram_chanels/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.telegram_chanels'), social_telegram_index_path(@social)
    set_meta('{name}', @telegram_chanel)
    add_breadcrumb @h1
  end

  # GET /telegram_chanels/new
  def new
    authorize TelegramChanel
    @telegram_chanel = TelegramChanel.new
    @telegram_chanel.social = @social

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.telegram_chanels'), social_telegram_index_path(@social)
    add_breadcrumb @h1
  end

  # GET /telegram_chanels/1/edit
  def edit
    authorize @telegram_chanel
    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.telegram_chanels'), social_telegram_index_path(@social)
    set_meta('{name}', @telegram_chanel)
    add_breadcrumb @h1
  end

  # POST /telegram_chanels
  def create
    authorize TelegramChanel
    @telegram_chanel = TelegramChanel.new(telegram_chanel_params)
    @telegram_chanel.social = @social

    if @telegram_chanel.save
      redirect_to social_telegram_path(@telegram_chanel.social, @telegram_chanel), notice: 'Telegram chanel was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /telegram_chanels/1
  def update
    authorize @telegram_chanel

    if @telegram_chanel.update(telegram_chanel_params)
      redirect_to social_telegram_path(@telegram_chanel.social, @telegram_chanel), notice: 'Telegram chanel was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /telegram_chanels/1
  def destroy
    authorize @telegram_chanel
    @telegram_chanel.destroy
    redirect_to social_telegram_index_url, notice: 'Telegram chanel was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_telegram_chanel
      @telegram_chanel = TelegramChanel.find_by(slug: params[:slug])
    end

    def set_social
      @social = Social.find_by(slug: params[:social_slug])
    end

    # Only allow a trusted parameter "white list" through.
    def telegram_chanel_params
      params.require(:telegram_chanel).permit(
        :name,
        :slug,
        :type_chanel,
        :body,
        :website,
        :count_user,
        :type,
        :tag_list,
        :partner_link
      )
    end
end
