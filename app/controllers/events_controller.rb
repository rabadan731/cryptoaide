class EventsController < ApplicationController
  before_action :set_money
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  # GET /events
  def index
    @events = Event.all

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    @page = set_meta(I18n.t('activerecord.models.events'))
    add_breadcrumb @h1
  end

  # GET /events/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.events'), money_events_path(@money)
    set_meta('{name}', @event)
    add_breadcrumb @h1
  end

  # GET /events/new
  def new
    authorize Event
    @event = Event.new
    @event.money_id = @money.id

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.events'), money_events_path(@money)
    add_breadcrumb @h1
  end

  # GET /events/1/edit
  def edit
    authorize @event
    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.events'), money_events_path(@money)
    set_meta('{name}', @event)
    add_breadcrumb @h1
  end

  # POST /events
  def create
    authorize Event
    @event = Event.new(event_params)
    @event.money = @money

    if @event.save
      redirect_to money_event_path(@money, @event), notice: 'Event was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /events/1
  def update
    authorize @event
    if @event.update(event_params)
      redirect_to money_event_path(@money, @event), notice: 'Event was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /events/1
  def destroy
    authorize @event
    @event.destroy
    redirect_to money_events_url(@money), notice: 'Event was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find_by(slug: params[:event_slug])
  end

  def set_money
    @money = Money.find_by(slug: params[:money_slug])
  end

  # Only allow a trusted parameter "white list" through.
  def event_params
    params.require(:event).permit(
      :money_id,
      :name,
      :body,
      :image,
      :appointment_date,
      :description,
      :status_text,
      :progress,
      :slug
    )
  end
end
