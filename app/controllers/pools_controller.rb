class PoolsController < ApplicationController
  before_action :set_pool, only: [:show, :edit, :update, :destroy]

  def go
    @pool = Pool.find_by(slug: params[:pool_slug])
    if @pool.partner_link.present?
      @pool.go_commit(request.remote_ip)
      redirect_to(@pool.partner_link)
    else
      if @pool.website.present?
        @pool.go_commit(request.remote_ip)
        redirect_to(@pool.website)
      else
        redirect_to pool_path(@pool)
      end
    end
  end

  # GET /pools
  def index
    @pools = Pool.all

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    set_meta(I18n.t('activerecord.models.pool'))
    add_breadcrumb @h1
  end

  # GET /pools/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.pool'), pools_path
    set_meta('{name}', @pool)
    add_breadcrumb @h1

    @solo_mining = @pool.pool_moneys.where(mining_type: :solo_mining)
    @multiple_mining = @pool.pool_moneys.where(mining_type: :multiple_mining)
  end

  # GET /pools/new
  def new
    authorize Pool
    @pool = Pool.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.pool'), pools_path
    add_breadcrumb @h1
  end

  # GET /pools/1/edit
  def edit
    authorize @pool
  end

  # POST /pools
  def create
    authorize Pool
    @pool = Pool.new(pool_params)

    if @pool.save
      redirect_to @pool, notice: 'Pool was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /pools/1
  def update
    authorize @pool
    if @pool.update(pool_params)
      redirect_to @pool, notice: 'Pool was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /pools/1
  def destroy
    authorize @pool
    @pool.destroy
    redirect_to pools_url, notice: 'Pool was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pool
      @pool = Pool.find_by_slug(params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def pool_params
      params.require(:pool).permit(:name, :body, :website, :slug, :pool_type)
    end
end