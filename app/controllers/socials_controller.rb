class SocialsController < ApplicationController
  before_action :set_social, only: [:show, :edit, :update, :destroy]

  add_breadcrumb I18n.t('breadcrumbs.homepage'), :root_path
  add_breadcrumb I18n.t('activerecord.models.socials'), :socials_path

  # GET /socials
  def index
    authorize Social

    @socials = Social.all

    @page = set_meta(I18n.t('activerecord.models.socials'))
  end

  # GET /socials/1
  def show
    authorize @social

    @youtube_chanels = @social.youtube_chanels
    @telegram_chanels = @social.telegram_chanels


    set_meta('{name}', @social)
    add_breadcrumb @h1
  end

  # GET /socials/new
  def new
    authorize Social

    @social = Social.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb @h1
  end

  # GET /socials/1/edit
  def edit
    authorize @social

    add_breadcrumb @social.name, social_path(@social)
    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb @h1
  end

  # POST /socials
  def create
    authorize Social
    @social = Social.new(social_params)

    if @social.save
      redirect_to @social, notice: 'Social was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /socials/1
  def update
    authorize @social
    if @social.update(social_params)
      redirect_to @social, notice: 'Social was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /socials/1
  def destroy
    authorize @social
    @social.destroy
    redirect_to socials_url, notice: 'Social was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_social
      @social = Social.find_by_slug(params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def social_params
      params.require(:social).permit(:name, :slug, :image, :body)
    end
end
