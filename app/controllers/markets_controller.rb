class MarketsController < ApplicationController
  before_action :set_market, only: [:show, :edit, :update, :destroy]

  def go
    @market = Market.find_by(slug: params[:market_slug])
    if @market.partner_link.present?
      @market.go_commit(request.remote_ip)
      redirect_to(@market.partner_link)
    else
      if @market.website.present?
        @market.go_commit(request.remote_ip)
        redirect_to(@market.website)
      else
        redirect_to market_path(@market)
      end
    end
  end

  # GET /markets
  def index
    @markets = Market.all

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    set_meta(I18n.t('activerecord.models.market'))
  end

  # GET /markets/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.market'), markets_path
    set_meta('{title}', @market)
  end

  # GET /markets/new
  def new
    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.market'), markets_path
    add_breadcrumb @h1

    authorize Market
    @market = Market.new
  end

  # GET /markets/1/edit
  def edit
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.payment_systems'), markets_path
    add_breadcrumb @market.title, market_path(@market)
    add_breadcrumb I18n.t('breadcrumbs.update')

    authorize @market
  end

  # POST /markets
  def create
    authorize Market
    @market = Market.new(market_params)

    if @market.save
      redirect_to @market, notice: 'Market was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /markets/1
  def update
    authorize @market
    if @market.update(market_params)
      redirect_to @market, notice: 'Market was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /markets/1
  def destroy
    authorize @market
    @market.destroy
    redirect_to markets_url, notice: 'Market was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_market
      @market = Market.find_by(slug: params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def market_params
      params.require(:market).permit(:title, :slug, :website, :image, :language, :body, :twitter, :tag_list)
    end
end
