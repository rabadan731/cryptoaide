class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  BRAND_NAME = 'CryptoAide.Net'.freeze

  def meta_title(title)
    [title, BRAND_NAME].reject(&:empty?).join(' | ')
  end

  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  layout :layout_by_resource

  private

  def layout_by_resource
    if devise_controller?
      "login"
    else
      "application"
    end
  end

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
    flash[:warning] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: :default
    redirect_to(request.referrer || root_path)
  end

  def set_meta(default_text, object = nil, others_replace = [])
    page = Page.find_or_create_by!(controller: controller_name, action: action_name).replace_data(object)

    @meta_description = (page.meta_description) ? (page.meta_description) : (default_text)
    @meta_keywors = page.meta_keywords
    @meta_title = (page.title.present?) ? (page.title) : (default_text)
    @h1 = (page.h1.present?) ? (page.h1) : (default_text)

    # подмена шаблонов в текстке
    [@meta_description, @meta_keywors, @meta_title, @h1].each do |content|
      next if content.nil?
      content.scan(/\{.*?}/).map do |replace_tag|
        method_name = (replace_tag.gsub('{', '').gsub('}', '')).to_sym
        if others_replace.count > 0 && others_replace.key?(method_name)
          content.gsub!(replace_tag, "#{others_replace[method_name]}")
        else
          if object.present? && object.respond_to?(method_name)
            content.gsub!(replace_tag, "#{object.send(method_name)}")
          end
        end
      end
    end

    page
  end
end
