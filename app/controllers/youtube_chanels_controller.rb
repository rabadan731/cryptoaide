class YoutubeChanelsController < ApplicationController
  before_action :set_youtube_chanel, only: [:show, :edit, :update, :destroy, :update_from_youtube]
  before_action :set_social

  def go
    @youtube_chanels = YoutubeChanel.find_by(slug: params[:youtube_chanel_slug])
    if @youtube_chanels.partner_link.present?
      @youtube_chanels.go_commit(request.remote_ip)
      redirect_to(@youtube_chanels.partner_link)
    else
      if @youtube_chanels.website.present?
        @youtube_chanels.go_commit(request.remote_ip)
        redirect_to(@youtube_chanels.website)
      else
        redirect_to social_youtube_path(@youtube_chanels)
      end
    end
  end

  # GET /youtube_chanels
  def index
    @youtube_chanels = YoutubeChanel.page params[:page]

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    set_meta(I18n.t('activerecord.models.youtube_chanels'))
    add_breadcrumb @h1
  end

  # GET /youtube_chanels/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.youtube_chanels'), social_youtube_index_path(@social)
    set_meta('{name}', @youtube_chanel)
    add_breadcrumb @h1
  end

  # GET /youtube_chanels/new
  def new
    authorize YoutubeChanel
    @youtube_chanel = YoutubeChanel.new
    @youtube_chanel.social = @social
    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.youtube_chanels'), social_youtube_index_path(@social)
    add_breadcrumb @h1
  end

  # GET /youtube_chanels/1/edit
  def update_from_youtube
    authorize @youtube_chanel

    channel = @youtube_chanel.yt

    @youtube_chanel.name = channel.title
    @youtube_chanel.image = channel.thumbnail_url(:high)
    @youtube_chanel.body = channel.description
    @youtube_chanel.count_user = channel.subscriber_count
    @youtube_chanel.slug = nil
    @youtube_chanel.save

    redirect_to @youtube_chanel, notice: 'Youtube updated.'
  end

  # GET /youtube_chanels/1/edit
  def edit
    authorize @youtube_chanel
    @youtube_chanel.social = @social
    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.youtube_chanels'), social_youtube_path(@social)
    set_meta('{name}', @youtube_chanel)
    add_breadcrumb @h1
  end

  # POST /youtube_chanels
  def create
    authorize YoutubeChanel
    @youtube_chanel = YoutubeChanel.new(youtube_chanel_params)
    @youtube_chanel.social = @social
    if @youtube_chanel.name.empty?
      @youtube_chanel.name = @youtube_chanel.chanel_id
    end
    if @youtube_chanel.website.nil? || @youtube_chanel.website.empty?
      @youtube_chanel.website = "https://www.youtube.com/channel/#{@youtube_chanel.chanel_id}"
    end

    if @youtube_chanel.save
      redirect_to social_youtube_path(@social, @youtube_chanel), notice: 'Youtube chanel was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /youtube_chanels/1
  def update
    authorize @youtube_chanel
    if @youtube_chanel.update(youtube_chanel_params)
      redirect_to social_youtube_path(@social, @youtube_chanel), notice: 'Youtube chanel was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /youtube_chanels/1
  def destroy
    authorize @youtube_chanel
    @youtube_chanel.destroy
    redirect_to social_url(@social), notice: 'Youtube chanel was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_youtube_chanel
    @youtube_chanel = YoutubeChanel.find_by(slug: params[:slug])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_social
    @social = Social.find_by(slug: params[:social_slug])
  end

  # Only allow a trusted parameter "white list" through.
  def youtube_chanel_params
    params.require(:youtube_chanel).permit(
      :name,
      :slug,
      :body,
      :website,
      :image,
      :count_user,
      :tag_list,
      :chanel_id,
      :partner_link
    )
  end
end
