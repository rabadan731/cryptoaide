class TelegramWebhooksController < Telegram::Bot::UpdatesController
  # use callbacks like in any other controllers
  around_action :with_locale

  # Every update can have one of: message, inline_query, chosen_inline_result,
  # callback_query, etc.
  # Define method with same name to respond to this updates.
  # def message(message)
  #   # message can be also accessed via instance method
  #   message == self.payload # true
  #   # store_message(message['text'])
  # end

  def message(message)
    respond_with :message, text: 'Mining GOOD!!'
  end

  # This basic methods receives commonly used params:
  #
  #   message(payload)
  #   inline_query(query, offset)
  #   chosen_inline_result(result_id, query)
  #   callback_query(data)

  # Define public methods to respond to commands.
  # Command arguments will be parsed and passed to the method.
  # Be sure to use splat args and default values to not get errors when
  # someone passed more or less arguments in the message.
  #
  # For some commands like /message or /123 method names should start with
  # `on_` to avoid conflicts.
  def start(data = nil, *)
    # do_smth_with(data)

    # There are `chat` & `from` shortcut methods.
    # For callback queries `chat` if taken from `message` when it's available.
    response = from ? "Hello 5 #{from['username']}!" : 'Hi there!'
    # There is `respond_with` helper to set `chat_id` from received message:
    respond_with :message, text: response
  end


  def courses(data = nil, *)
    text = ''
    Money.is_crypto.order('market_cap_usd DESC').limit(10).all.each do |money|
      text += "##{money.symbol}: #{money.price_usd} (#{money.percent_change_24h}%) \n"
    end
    respond_with :message, text: text
  end

  def news(data = nil, *)
    News.latest.reverse.each do |news|
      respond_with :message, text: news.link
      sleep 1
    end
  end

  private

  def with_locale(&block)
    I18n.with_locale(locale_for_update, &block)
  end

  def locale_for_update
    if from
      :ru
    elsif chat
      :ru
    end
  end
end