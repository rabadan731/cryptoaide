class ChangeController < ApplicationController
  include MoneyHelper
  TYPE_ALL = 'ALL'
  TYPE_CRYPTO = 'CRYPTO'
  # GET /change/money_from/money_to
  def change
    success_currency = %w(ALL CRYPTO) + fiat_list.map(&:to_s)

    @moneys_from = Money.change_sell
    @moneys_to = Money.change_buy

    @changes_params = {
        currency_from: TYPE_ALL,
        money_from: TYPE_ALL,
        currency_to: TYPE_ALL,
        money_to: TYPE_ALL,
    }

    exchange_course_find = ExchangeCourse.where("exchange_courses.created_at > ?", 15.minutes.ago)
      .where("exchange_courses.value_out > 0")
      .where("exchange_courses.value_in > 0")


    unless params[:money_from].nil? || params[:money_from] == TYPE_ALL
      from_money = Money.find_by(symbol: params[:money_from])
      @changes_params[:money_from] = from_money.symbol
      exchange_course_find = exchange_course_find.where(
        money_from: from_money
      )
      if params[:currency_from] != TYPE_ALL &&
        (
          (params[:currency_from] == TYPE_CRYPTO && from_money.is_crypto? == false) ||
          (params[:currency_from] != TYPE_CRYPTO && from_money.is_crypto?) ||
          (params[:currency_from] != TYPE_CRYPTO &&
            from_money.is_crypto? == false &&
            params[:currency_from] != from_money.currency
          )
        ) &&
        params[:currency_from] != from_money.currency
        return redirect_to(chenge_path(
                      (from_money.is_crypto?)?(TYPE_CRYPTO):(from_money.currency),
                      params[:money_from],
                      params[:currency_to],
                      params[:money_to]), status: 301)
      end
    end

    unless params[:money_to].nil? || params[:money_to] == TYPE_ALL
      to_money = Money.find_by(symbol:params[:money_to])
      @changes_params[:money_to] = to_money.symbol
      exchange_course_find = exchange_course_find.where(
        money_to: to_money
      )

      if params[:currency_to] != TYPE_ALL &&
        (
          (params[:currency_to] == TYPE_CRYPTO && to_money.is_crypto? == false) ||
          (params[:currency_to] != TYPE_CRYPTO && to_money.is_crypto?) ||
          (params[:currency_to] != TYPE_CRYPTO &&
            to_money.is_crypto? == false &&
            params[:currency_to] != to_money.currency
          )
        )
        return redirect_to(chenge_path(
                      params[:currency_from],
                      params[:money_from],
                      (to_money.is_crypto?)?(TYPE_CRYPTO):(to_money.currency),
                      params[:money_to]), status: 301)
      end
    end

    unless (success_currency.include?(params[:currency_from])) && (success_currency.include?(params[:currency_to]))
      path1 = success_currency.include?(params[:currency_from])?(params[:currency_from]):(
        (from_money.present? && from_money.is_crypto?)?(TYPE_CRYPTO):(TYPE_ALL)
      )
      path2 = (params[:money_from].present?)?(params[:money_from]):(TYPE_ALL)
      path3 = success_currency.include?(params[:currency_to])?(params[:currency_to]):(
        (to_money.present? && to_money.is_crypto?)?(TYPE_CRYPTO):(TYPE_ALL)
      )
      path4 = (params[:money_to].present?)?(params[:money_to]):(TYPE_ALL)

      return redirect_to(chenge_path(path1, path2, path3, path4), status: 301)
    end


    unless params[:currency_from].nil? || params[:currency_from] == TYPE_ALL
      @changes_params[:currency_from] = params[:currency_from]
      if params[:currency_from] == TYPE_CRYPTO
        exchange_course_find = exchange_course_find.left_joins(:money_from).where(
          money: { money_type_id: 1 }
        )
      else
        exchange_course_find = exchange_course_find.where(
          currency_from: @changes_params[:currency_from]
        )
      end
    end

    unless params[:currency_to].nil? || params[:currency_to] == TYPE_ALL
      @changes_params[:currency_to] = params[:currency_to]

      if params[:currency_to] == TYPE_CRYPTO
        exchange_course_find = exchange_course_find.left_joins(:money_to).where(
          money: { money_type_id: 1 }
        )
      else
        exchange_course_find = exchange_course_find.where(
          currency_to: @changes_params[:currency_to]
        )
      end
    end

    @currency_list = Money.currency_list.all
    @exchange_course = exchange_course_find
       .includes(:exchange)
       .includes(:money_to)
       .includes(:money_from)
       .limit(25)
       .order('(value_in*1.0001)/(value_out*1.0001)')
       .all

    @money_from_title = (@changes_params[:money_from] == TYPE_ALL)?
                          (t("crypto.#{@changes_params[:currency_from]}")):
                          Money.find_by_symbol(@changes_params[:money_from]).name

    @money_to_title = (@changes_params[:money_to] == TYPE_ALL)?
                        (t("crypto.#{@changes_params[:currency_to]}")):
                        Money.find_by_symbol(@changes_params[:money_to]).name

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.exchange'), exchanges_path
    set_meta("Лучший обмен #{@money_from_title} на #{@money_to_title}")

    @first_course = @exchange_course.first

    respond_to do |format|
      format.html
      format.js
    end
  end


  private

  def chek_url

  end
end
