class NewsSourcesController < ApplicationController
  before_action :set_news_source, only: [:show, :edit, :update, :destroy]

  # GET /news_sources
  def index
    authorize NewsSource
    @news_sources = NewsSource.all
  end

  # GET /news_sources/1
  def show
    authorize @news_source
  end

  # GET /news_sources/new
  def new
    authorize NewsSource
    @news_source = NewsSource.new
  end

  # GET /news_sources/1/edit
  def edit
    authorize @news_source
  end

  # POST /news_sources
  def create
    authorize NewsSource
    @news_source = NewsSource.new(news_source_params)

    if @news_source.save
      redirect_to @news_source, notice: 'News source was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /news_sources/1
  def update
    authorize @news_source
    if @news_source.update(news_source_params)
      redirect_to @news_source, notice: 'News source was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /news_sources/1
  def destroy
    authorize @news_source
    @news_source.destroy
    redirect_to news_sources_url, notice: 'News source was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news_source
      @news_source = NewsSource.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def news_source_params
      params.require(:news_source).permit(:name, :source, :website, :status)
    end
end
