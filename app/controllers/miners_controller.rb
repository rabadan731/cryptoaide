class MinersController < ApplicationController
  before_action :set_miner, only: [:show, :edit, :update, :destroy]

  add_breadcrumb I18n.t('breadcrumbs.homepage'), :root_path
  add_breadcrumb I18n.t('activerecord.models.miner'), :miners_path

  def import_nicehash
    require 'parser/nicehash_parser'
    pars = NicehashParser.new
    pars.miners
  end

  def import
    if params[:miner][:file].present?
      return redirect_to miners_url, notice: 'Not loaded file!'
    end

    require 'csv'
    require 'open-uri'

    csv = CSV.parse(
        File.read(open(params[:miner][:file].tempfile)),
        :headers => true,
        quote_char: '"',
        col_sep: ';'
    )

    save_success = 0
    save_error = 0

    csv.each do |row|
      if Miner.create(row.to_hash)
        save_success += 1
      else
        save_error += 1
      end
    end

    redirect_to miners_url, notice: "Miners imported: #{save_success}, error: #{save_error}"
  end

  # GET /miners
  def index
    @miners = Miner.all

    set_meta(I18n.t('activerecord.models.miner'))
    add_breadcrumb @h1
  end

  # GET /miners/1
  def show
    set_meta('{name}', @miner)
    add_breadcrumb @h1
  end

  # GET /miners/new
  def new
    authorize Miner
    @miner = Miner.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb @h1
  end

  # GET /miners/1/edit
  def edit
    authorize @miner

    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb @h1
  end

  # POST /miners
  def create
    authorize Miner
    @miner = Miner.new(miner_params)

    if @miner.save
      redirect_to @miner, notice: 'Miner was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /miners/1
  def update
    authorize @miner
    if @miner.update(miner_params)
      redirect_to @miner, notice: 'Miner was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /miners/1
  def destroy
    authorize @miner
    @miner.destroy
    redirect_to miners_url, notice: 'Miner was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_miner
    @miner = Miner.find_by_slug(params[:slug])
  end

  # Only allow a trusted parameter "white list" through.
  def miner_params
    params.require(:miner).permit(:name, :body, :slug, :image, :miner_type, :power_rate, :noise_level)
  end
end
