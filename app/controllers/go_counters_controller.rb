class GoCountersController < ApplicationController
  before_action :set_go_counter, only: [:show, :edit, :update, :destroy]

  # GET /go_counters
  def index
    authorize GoCounter
    @go_counters = GoCounter
                     .select('resource_class, resource_id, MAX(date_go) as last_date, count(resource_id) as count_click')
                     .group('resource_class, resource_id')
                     .all
  end

  # GET /go_counters/1
  def showcount_click
    authorize GoCounter
  end

  # GET /go_counters/new
  def new
    authorize GoCounter
    @go_counter = GoCounter.new
  end

  # GET /go_counters/1/edit
  def edit
    authorize @go_counter
  end

  # POST /go_counters
  def create
    authorize GoCounter
    @go_counter = GoCounter.new(go_counter_params)

    if @go_counter.save
      redirect_to @go_counter, notice: 'Go counter was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /go_counters/1
  def update
    authorize @go_counter
    if @go_counter.update(go_counter_params)
      redirect_to @go_counter, notice: 'Go counter was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /go_counters/1
  def destroy
    authorize @go_counter
    @go_counter.destroy
    redirect_to go_counters_url, notice: 'Go counter was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_go_counter
      @go_counter = GoCounter.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def go_counter_params
      params.require(:go_counter).permit(:resource_class, :resource_id, :date_go, :user_agent, :ip)
    end
end
