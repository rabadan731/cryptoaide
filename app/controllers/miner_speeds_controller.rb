class MinerSpeedsController < ApplicationController
  before_action :set_miner_speed, only: [:show, :edit, :update, :destroy]
  before_action :set_miner

  add_breadcrumb I18n.t('breadcrumbs.homepage'), :root_path
  add_breadcrumb I18n.t('activerecord.models.miner'), :miners_path

  # GET /miner_speeds
  def index
    @miner_speeds = MinerSpeed.all
  end

  # GET /miner_speeds/1
  def show
    add_breadcrumb "#{@miner.name}", miner_path(@miner)
    add_breadcrumb @miner_speed.name
    set_meta('{name} - {algorithm}', @miner, {algorithm: @miner_speed.name})
  end

  # GET /miner_speeds/new
  def new
    authorize MinerSpeed
    @miner_speed = MinerSpeed.new
    @miner_speed.miner = @miner

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('activerecord.models.miner'), miner_path(@miner)
    add_breadcrumb @h1
  end

  # GET /miner_speeds/1/edit
  def edit
    authorize @miner_speed
  end

  # POST /miner_speeds
  def create
    authorize MinerSpeed
    @miner_speed = MinerSpeed.new(miner_speed_params)
    @miner_speed.miner = @miner

    if @miner_speed.save
      redirect_to miner_speed_path(@miner_speed.miner, @miner_speed),
                  notice: 'Miner speed was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /miner_speeds/1
  def update
    authorize @miner_speed
    if @miner_speed.update(miner_speed_params)
      redirect_to miner_speed_path(@miner_speed.miner, @miner_speed),
                  notice: 'Miner speed was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /miner_speeds/1
  def destroy
    authorize @miner_speed
    @miner_speed.destroy
    redirect_to miner_url(@miner), notice: 'Miner speed was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_miner_speed
      @miner_speed = MinerSpeed.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def miner_speed_params
      params.require(:miner_speed).permit(
        :miner_id,
        :algorithm_id,
        :algorithm_dual_id,
        :hash_rate,
        :hash_rate_dual,
        :power_rate
      )
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_miner
      @miner = Miner.find_by_slug(params[:miner_slug])
    end
end
