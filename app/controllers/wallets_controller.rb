class WalletsController < ApplicationController
  before_action :set_wallet, only: [:show, :edit, :update, :destroy]

  def go
    @wallet = Wallet.find_by(slug: params[:wallet_slug])
    if @wallet.partner_link.present?
      @wallet.go_commit(request.remote_ip)
      redirect_to(@wallet.partner_link)
    else
      if @wallet.website.present?
        @wallet.go_commit(request.remote_ip)
        redirect_to(@wallet.website)
      else
        redirect_to wallet_path(@wallet)
      end
    end
  end

  # GET /wallets
  def index
    @wallets = Wallet.all

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    set_meta(I18n.t('activerecord.models.wallet'))
    add_breadcrumb @h1
  end

  # GET /wallets/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.wallet'), wallets_path
    set_meta('{name}', @wallet)
    add_breadcrumb @h1
  end

  # GET /wallets/new
  def new
    authorize Wallet
    @wallet = Wallet.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.wallet'), wallets_path
    add_breadcrumb @h1
  end

  # GET /wallets/1/edit
  def edit
    authorize @wallet

    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.wallet'), wallets_path
    set_meta('{name}', @wallet)
    add_breadcrumb @h1
  end

  # POST /wallets
  def create
    authorize Wallet
    @wallet = Wallet.new(wallet_params)

    if @wallet.save
      redirect_to @wallet, notice: 'Wallet was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /wallets/1
  def update
    authorize @wallet

    if @wallet.update(wallet_params)
      redirect_to @wallet, notice: 'Wallet was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /wallets/1
  def destroy
    authorize @wallet
    @wallet.destroy
    redirect_to wallets_url, notice: 'Wallet was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wallet
      @wallet = Wallet.find_by_slug(params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def wallet_params
      params.require(:wallet).permit(:name, :slug, :body, :website, :partner_link)
    end
end
