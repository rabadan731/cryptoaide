class ExchangesController < ApplicationController
  before_action :set_exchange, only: [:go, :show, :edit, :update, :destroy]

  # GET /exchanges
  def index
    @exchanges = Exchange.all

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    set_meta(I18n.t('activerecord.models.exchange'))
  end

  def go
    @exchange = Exchange.find_by(slug: params[:exchange_slug])
    if @exchange.partner_link.present?
      @exchange.go_commit(request.remote_ip)
      redirect_to(@exchange.partner_link)
    else
      if @exchange.website.present?
        @exchange.go_commit(request.remote_ip)
        redirect_to(@exchange.website)
      else
        redirect_to exchange_path(@exchange)
      end
    end
  end

  # GET /exchanges
  def load_courses
    LoadCoursesJob.perform_later
    redirect_to exchanges_url, notice: 'Courses loaded start'
  end

  # GET /exchanges
  def load_course
    require 'parser/course'
    @exchange = Exchange.find_by(slug: params[:exchange_slug])
    # LoadCoursesJob.perform_later
    Course.new(@exchange).parse
    redirect_to exchange_path(@exchange), notice: 'Courses loaded start'
  end

  # GET /exchanges/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.exchange'), exchanges_path
    set_meta('{title}', @exchange)
  end

  # GET /exchanges/new
  def new
    authorize Exchange
    @exchange = Exchange.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.exchanges'), exchanges_path
    add_breadcrumb @h1
  end

  # GET /exchanges/1/edit
  def edit
    authorize @exchange

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.exchange'), exchanges_path
    add_breadcrumb @exchange.title, exchange_path(@exchange)
    add_breadcrumb I18n.t('breadcrumbs.update')
  end

  # POST /exchanges
  def create
    authorize Exchange
    @exchange = Exchange.new(exchange_params)

    if @exchange.save
      redirect_to @exchange, notice: 'Exchange was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /exchanges/1
  def update
    authorize @exchange
    if @exchange.update(exchange_params)
      redirect_to @exchange, notice: 'Exchange was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /exchanges/1
  def destroy
    authorize @exchange
    @exchange.destroy
    redirect_to exchanges_url, notice: 'Exchange was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_exchange
    @exchange = Exchange.find_by(slug: params[:slug])
  end

  # Only allow a trusted parameter "white list" through.
  def exchange_params
    params.require(:exchange).permit(
      :title,
      :slug,
      :body,
      :website,
      :partner_link,
      :status,
      :image,
      :country_id,
      :exchange_slug,
      :course_link,
      :parser_type,
      :birth_date,
      :tag_list
    )
  end
end
