class ArticleCategoriesController < ApplicationController
  before_action :set_article_category, only: [:show, :edit, :update, :destroy]

  # GET /article_categories
  def index
    @article_categories = ArticleCategory.all
  end

  # GET /article_categories/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.article_category'), article_categories_path
    set_meta('{title}', @article_category)
  end

  # GET /article_categories/new
  def new
    authorize ArticleCategory
    @article_category = ArticleCategory.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.article_category'), article_categories_path
    add_breadcrumb @h1
  end

  # GET /article_categories/1/edit
  def edit
    authorize @article_category
  end

  # POST /article_categories
  def create
    authorize ArticleCategory
    @article_category = ArticleCategory.new(article_category_params)

    if @article_category.save
      redirect_to @article_category, notice: 'Article category was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /article_categories/1
  def update
    authorize @article_category
    if @article_category.update(article_category_params)
      redirect_to @article_category, notice: 'Article category was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /article_categories/1
  def destroy
    authorize @article_category
    @article_category.destroy
    redirect_to article_categories_url, notice: 'Article category was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article_category
      @article_category = ArticleCategory.find_by(slug: params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def article_category_params
      params.require(:article_category).permit(:title, :body, :slug, :meta_desc, :meta_key, :image)
    end
end
