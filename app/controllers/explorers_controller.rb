class ExplorersController < ApplicationController
  before_action :set_money
  before_action :set_explorer, only: [:show, :edit, :update, :destroy]

  # GET /explorers
  def index
    @explorers = Explorer.all

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    set_meta(I18n.t('activerecord.models.explorers'))
    add_breadcrumb @h1
  end

  # GET /explorers/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.explorers'), money_explorers_path(@money)
    set_meta('{name}', @explorer)
    add_breadcrumb @h1
  end

  # GET /explorers/new
  def new
    authorize Explorer
    @explorer = Explorer.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.explorers'), money_explorers_path(@money)
    add_breadcrumb @h1
  end

  # GET /explorers/1/edit
  def edit
    authorize @explorer
    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.explorers'), money_explorers_path(@money)
    set_meta('{name}', @explorer)
    add_breadcrumb @h1
  end

  # POST /explorers
  def create
    authorize Explorer
    @explorer = Explorer.new(explorer_params)
    @explorer.money = @money

    if @explorer.save
      redirect_to money_explorer_path(@money,@explorer), notice: 'Explorer was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /explorers/1
  def update
    authorize @explorer
    if @explorer.update(explorer_params)
      redirect_to money_explorer_path(@money,@explorer), notice: 'Explorer was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /explorers/1
  def destroy
    authorize @explorer
    @explorer.destroy
    redirect_to money_explorers_url(@money), notice: 'Explorer was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_explorer
    @explorer = Explorer.find_by(slug: params[:explorer_slug])
  end

  def set_money
    @money = Money.find_by(slug: params[:money_slug])
  end

  # Only allow a trusted parameter "white list" through.
  def explorer_params
    params.require(:explorer).permit(:name, :body, :slug, :image, :money_id, :website, :api_url, :api_type)
  end
end
