require 'parser/coinmarketcap'
class PageController < ApplicationController
  def index
    @news = News.latest

    @moneys = Money.is_crypto.order('market_cap_usd DESC').limit(20).all

    set_meta('Твой криптовалютный помощник!')
  end
end
