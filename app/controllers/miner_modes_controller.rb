class MinerModesController < ApplicationController
  before_action :set_miner_mode, only: [:show, :edit, :update, :destroy]
  before_action :set_miner

  add_breadcrumb I18n.t('breadcrumbs.homepage'), :root_path
  add_breadcrumb I18n.t('activerecord.models.miner'), :miners_path

  # GET /miner_modes
  def index
    @miner_modes = MinerMode.all
  end

  # GET /miner_modes/1
  def show
    add_breadcrumb "#{@miner.name}", miner_path(@miner)
    add_breadcrumb @miner_mode.name
    set_meta('{name} - {moneys}', @miner, {moneys: @miner_mode.name})
  end

  # GET /miner_modes/new
  def new
    authorize MinerMode
    @miner_mode = MinerMode.new
    @miner_mode.miner = @miner

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('activerecord.models.miner'), miner_path(@miner)
    add_breadcrumb @h1
  end

  # GET /miner_modes/1/edit
  def edit
    authorize @miner_mode
  end

  # POST /miner_modes
  def create
    authorize MinerMode
    @miner_mode = MinerMode.new(miner_mode_params)
    @miner_mode.miner = @miner

    if @miner_mode.save
      redirect_to miner_mode_path(@miner_mode.miner, @miner_mode),
                  notice: 'Miner mode was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /miner_modes/1
  def update
    authorize @miner_mode
    if @miner_mode.update(miner_mode_params)
      redirect_to miner_mode_path(@miner_mode.miner, @miner_mode), notice: 'Miner mode was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /miner_modes/1
  def destroy
    authorize @miner_mode
    @miner_mode.destroy
    redirect_to miner_url(@miner), notice: 'Miner mode was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_miner_mode
    @miner_mode = MinerMode.find_by_slug(params[:slug])
  end

  # Only allow a trusted parameter "white list" through.
  def miner_mode_params
    params.require(:miner_mode).permit(:name, :slug, :image, :body, :miner_id, :website, :partner_link, :price)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_miner
    @miner = Miner.find_by_slug(params[:miner_slug])
  end
end
