require 'parser/whattomine_parse'
class AlgorithmsController < ApplicationController
  before_action :set_algorithm, only: [:show, :edit, :update, :destroy]

  # GET /algorithms
  def index
    @algorithms = Algorithm.order('name').all

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    set_meta(I18n.t('activerecord.models.algorithm'))
    add_breadcrumb @h1
  end


  # GET /algorithms
  def import
    WhattomineParse.load_algorithms

    redirect_to algorithms_url, notice: 'Algorithms was successfully imported.'
  end

  # GET /algorithms/1
  def show
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.algorithm'), algorithms_path
    set_meta('{name}', @algorithm)
    add_breadcrumb @h1
  end

  # GET /algorithms/new
  def new
    authorize Algorithm
    @algorithm = Algorithm.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.algorithm'), algorithms_path
    add_breadcrumb @h1
  end

  # GET /algorithms/1/edit
  def edit
    authorize @algorithm

    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.algorithm'), algorithms_path
    set_meta('{name}', @algorithm)
    add_breadcrumb @h1
  end

  # POST /algorithms
  def create
    authorize Algorithm
    @algorithm = Algorithm.new(algorithm_params)

    if @algorithm.save
      redirect_to @algorithm, notice: 'Algorithm was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /algorithms/1
  def update
    authorize @algorithm

    if @algorithm.update(algorithm_params)
      redirect_to @algorithm, notice: 'Algorithm was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /algorithms/1
  def destroy
    authorize @algorithm
    @algorithm.destroy
    redirect_to algorithms_url, notice: 'Algorithm was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_algorithm
      @algorithm = Algorithm.find_by(slug: params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def algorithm_params
      params.require(:algorithm).permit(:name, :slug, :body, :image, :tag_list)
    end
end
