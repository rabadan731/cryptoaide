class MoneyController < ApplicationController
  before_action :set_money, only: [:events, :show, :mining, :news, :edit, :update, :destroy]

  add_breadcrumb I18n.t('breadcrumbs.homepage'), :root_path
  add_breadcrumb I18n.t('activerecord.models.money'), :money_index_path

  # GET /money
  def index
    @moneys = Money.is_crypto
                   .where('market_cap_usd > 0')
                   .order(market_cap_usd: :desc)
                   .page params[:page]

    @money_fiats = Money.is_not_crypto.all

    @page = set_meta(I18n.t('activerecord.models.money'))
    add_breadcrumb @h1
  end

  # GET /money
  def top
    case params[:type]
    when 'best_price'
      order_money = { price_usd: :desc }
    when 'change_day'
      order_money = { percent_change_24h: :desc }
    when 'change_week'
      order_money = { percent_change_7d: :desc }
    when 'market_cap'
      order_money = { market_cap_usd: :desc }
    else
      redirect_to money_index_url, notice: 'Error page'
    end

    set_meta('{order}', @moneys, {order: t("money.sort.#{params[:type]}")})
    add_breadcrumb @h1
    @moneys = Money.is_crypto.order(order_money).page params[:page]
  end

  def events
    add_breadcrumb @money.name, money_path(@money)
    @page = set_meta('События')
    add_breadcrumb @h1

    @events = @money.events
  end

  def news
    add_breadcrumb @money.name, money_path(@money)
    @page = set_meta(t('activerecord.models.news'))
    @news = News.latest.tagged_with(@money.tag_list, :any => true).page params[:page]
    add_breadcrumb @h1
  end

  def mining
    @miners = @money.miners # оборудование
    @pools = @money.pools # пуллы

    @wallets = @money.wallets # кошельки
    @markets = @money.markets # кошельки

    add_breadcrumb @money.name, money_path(@money)
    page = set_meta('Майнинг {name} ({symbol})', @money)
    add_breadcrumb @h1

    @texts = {
        page: page,
        start: Page.find_or_create_by!(controller: 'money-mining', action: 'start').replace_data(@money),
        miners: Page.find_or_create_by!(controller: 'money-mining', action: 'miners').replace_data(@money),
        soft: Page.find_or_create_by!(controller: 'money-mining', action: 'soft').replace_data(@money),
        pool: Page.find_or_create_by!(controller: 'money-mining', action: 'pool').replace_data(@money),
        market: Page.find_or_create_by!(controller: 'money-mining', action: 'market').replace_data(@money),
        withdraw: Page.find_or_create_by!(controller: 'money-mining', action: 'withdraw').replace_data(@money)
    }
  end

  # GET /money/1
  def show
    @courses_buy_crypto = ExchangeCourse.courses_buy(@money).where.not(currency_from: fiat_list)
    @courses_buy_fiat = ExchangeCourse.courses_buy(@money).where(currency_from: fiat_list)
    @courses_sell_crypto = ExchangeCourse.courses_sell(@money).where.not(currency_to: fiat_list)
    @courses_sell_fiat = ExchangeCourse.courses_sell(@money).where(currency_to: fiat_list)
    @news = News.latest.tagged_with(@money.tag_list, :any => true)
    @markets = []

    set_meta('{name}', @money)
    add_breadcrumb @h1
  end

  # GET /money/new
  def new
    authorize Money
    @money = Money.new

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb @h1
  end

  # GET /money/1/edit
  def edit
    authorize @money

    add_breadcrumb @money.name, money_path(@money)
    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb @h1
  end

  # POST /money
  def create
    authorize Money
    @money = Money.new(money_params)

    if @money.save
      redirect_to @money, notice: 'Money was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /money/1
  def update
    authorize @money
    if @money.update(money_params)
      redirect_to @money, notice: 'Money was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /money/1
  def destroy
    authorize @money
    @money.destroy
    redirect_to money_index_url, notice: 'Money was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_money
    @money = Money.find_by(slug: params[:slug])
  end

  def fiat_list
    [
      :RUB,
      :USD,
      :EUR,
      :KZT,
      :UAH,
    ]
  end

  # Only allow a trusted parameter "white list" through.
  def money_params
    params.require(:money).permit(
      :name,
      :slug,
      :body,
      :currency,
      :website,
      :image,
      :symbol,
      :twitter,
      :money_type_id,
      :change_info_coefficient,
      :tag_list,
      :price_usd,
      :last_updated,
      :price_btc,
      :day_volume_usd,
      :market_cap_usd,
      :available_supply,
      :total_supply,
      :max_supply,
      :percent_change_1h,
      :percent_change_24h,
      :percent_change_7d,
    )
  end
end
