class UsersController < ApplicationController
  before_action :set_user, only: [:finish_signup, :show, :audit]

  # GET /users/:id.:format
  def show
    authorize @user

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.user')
    set_meta('{name}', @user)
    add_breadcrumb @h1
  end

  # GET /users/:id.:format/audit
  def audit
    authorize @user

    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.user')
    add_breadcrumb @user.name
    set_meta('Аудит')
    add_breadcrumb @h1

    @audits = Audited::Audit.
      where(user_type: 'User', user_id: @user.id).
      order(created_at: :desc).page params[:page]
  end

  # GET/PATCH /users/:id/finish_signup
  def finish_signup
    if request.patch? && params[:user] #&& params[:user][:email]
      if @user.update(user_params)
        @user.skip_reconfirmation!
        sign_in(@user, :bypass => true)
        redirect_to @user, notice: 'Your profile was successfully updated.'
      else
        @show_errors = true
      end
    end
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    accessible = [ :name, :email ] # extend with your own params
    accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
    params.require(:user).permit(accessible)
  end
end