class NewsController < ApplicationController
  before_action :set_news, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumbs, only: [:show, :edit, :new]

  # GET /news
  def index
    @news = News.order(created_at: :desc).page params[:page]

    @page = set_meta(I18n.t('activerecord.models.news'))
  end


  def go
    @news = News.find_by(slug: params[:news_slug])
    if @news.link.present?
      redirect_to(@news.link)
    else
      redirect_to news_path(@news)
    end
  end

  # GET /news/1
  def show
    add_breadcrumb @news.title
    @meta_title = meta_title @news.title
    @meta_description = @news.meta_description
    @meta_keywors = @news.meta_keywors

    @og_properties = {
      title: @meta_title,
      type:  'article',
      image: view_context.image_url(@news.image),  # this file should exist in /app/assets/images/logo.png
      url: news_url(@news)
    }
  end

  # GET /news/new
  def new
    authorize News
    @news = News.new
    @news.user = current_user

    @h1 = I18n.t('breadcrumbs.create')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.news'), news_index_path
    add_breadcrumb @h1
  end

  # GET /news/1/edit
  def edit
    authorize @news
    @news.user = current_user

    @h1 = I18n.t('breadcrumbs.update')
    add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
    add_breadcrumb I18n.t('activerecord.models.news'), news_index_path
    set_meta('{name}', @news)
    add_breadcrumb @h1
  end

  # POST /news
  def create
    authorize News
    @news = News.new(news_params)
    @news.user = current_user

    if @news.save
      redirect_to @news, notice: 'News was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /news/1
  def update
    authorize @news
    @news.user = current_user
    if @news.update(news_params)
      redirect_to @news, notice: 'News was successfully updated.'
    else
      add_breadcrumb @news.title, news_path(@news)
      add_breadcrumb I18n.t("breadcrumbs.update")
      render :edit
    end
  end

  # DELETE /news/1
  def destroy
    authorize @news
    News.destroy(@news.id)
    redirect_to news_index_url, notice: 'News was successfully destroyed.'
  end

  private
    def set_breadcrumbs
      add_breadcrumb I18n.t('breadcrumbs.homepage'), root_path
      add_breadcrumb I18n.t('activerecord.models.news'), news_index_path
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_news
      @news = News.find_by(slug: params[:slug])
    end

    # Only allow a trusted parameter "white list" through.
    def news_params
      params.require(:news).permit(
        :title,
        :meta_description,
        :meta_keywors,
        :slug,
        :body,
        :source,
        :link,
        :tag_list,
        :image
      )
    end
end
