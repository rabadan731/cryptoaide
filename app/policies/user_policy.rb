class UserPolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def show?
    !@user.nil? && scope.where(:id => @record.id).exists? && (@user.admin? || @user == @record)
  end

  def audit?
    !@user.nil? && scope.where(:id => @record.id).exists? && @user.admin?
  end
end
