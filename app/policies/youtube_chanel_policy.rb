class YoutubeChanelPolicy < ApplicationPolicy
  def update_from_youtube?
    @user.admin? if @user
  end
end
