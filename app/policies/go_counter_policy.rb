class GoCounterPolicy < ApplicationPolicy
  def index?
    @user.admin? if @user
  end

  def show?
    @user.admin? if @user
  end

  def create?
    @user.admin? if @user
  end

  def new?
    @user.admin? if @user
  end

  def update?
    @user.admin? if @user
  end

  def edit?
    @user.admin? if @user
  end

  def destroy?
    if @user
      @user.admin? || (@record.respond_to?(:user) && @record.user.present? && @user == @record.user)
    end
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
