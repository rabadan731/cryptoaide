$(document).ready(function () {
  init_exchange_filter();
});

function init_exchange_filter() {
  $('#money_from').isotope({
    itemSelector: '.grid-item',
    layoutMode: 'fitRows',
    filter: $('#money_from').data('selcted')
  });

  $('#money_to').isotope({
    itemSelector: '.grid-item',
    layoutMode: 'fitRows',
    filter: $('#money_to').data('selcted')
  });

}