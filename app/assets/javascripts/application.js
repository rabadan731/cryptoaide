// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//= require jquery-3.1.1.min
//= require jquery_ujs
//= require bootstrap.min.js
//= require ckeditor/init
//= require plugins/bootstrap-tagsinput/bootstrap-tagsinput
//= require news.js
//= require inspinia.js
//= require exchanges
//= require plugins/metisMenu/jquery.metisMenu.js
//= require plugins/pace/pace.min.js
//= require plugins/masonary/masonry.pkgd.min.js
//= require plugins/slimscroll/jquery.slimscroll.min.js
//= require plugins/peity/jquery.peity.min.js
//= require plugins/morris/raphael-2.1.0.min.js
//= require plugins/morris/morris.js
//= require cable.js
//= require plugins/isotope/isotope.pkgd.min


